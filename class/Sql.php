<?php

class Sql extends PDO {

    private $conn;

    public function __construct() {
        //vrclic.com.br
        $this->conn = new PDO("mysql:host=localhost;dbname=u334512707_vrcli", "u334512707_vrc", "AmFQC5;dm:meFsI@0T");
        //vrclic.com
        //$this->conn = new PDO("mysql:host=localhost;dbname=u488154127_vrcli", "u488154127_vrc", "AmFQC5;dm:meFsI@0T");
        //$this->conn = new PDO("mysql:host=sql131.main-hosting.eu;dbname=u488154127_vrcli", "u488154127_vrc", "AmFQC5;dm:meFsI@0T");
        //$this->conn = new PDO("mysql:host=localhost;dbname=u488154127_vrcli", "root", "123456");
        //$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    private function setParam($statment, $key, $value) {
        $statment->bindParam($key, $value);
    }

    private function setParams($statment, $parameters = array()) {
        foreach ($parameters as $key => $value) {
            $this->setParam($statment, $key, $value);
        }
    }

    public function query($rawQuery, $params = array()) {
        $stmt = $this->conn->prepare($rawQuery);
        $this->setParams($stmt, $params);
        
//        var_dump($stmt);
//        echo '<br/>';
//        var_dump($params);
//        echo '<br/>';
        //exit();
        
        $stmt->execute();

        return $stmt;
    }

    public function select($rawQuery, $params = array()) {
        $stmt = $this->query($rawQuery, $params);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectCount($rawQuery, $params = array()) {
        $stmt = $this->query($rawQuery, $params);

        return $stmt->fetchColumn();
    }

}

?>
