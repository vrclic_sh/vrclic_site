<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DateConverter
 *
 * @author marcos
 */
class TokenManager {

    public static function token_encode() {
        return md5(uniqid(rand(), true));
    }

    public static function geradorSenha($len = 5) {
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $base = strlen($charset);
        $result = '';
        $now = explode(' ', microtime())[1];
        while ($now >= $base) {
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
        }
        return substr($result, ($len*-1));
    }
    
    public static function passwordTransform($pass){
        return md5($pass);
    }
    public static function passwordCompare($pass, $hash){
        return md5($pass) == $hash;
    }

}
