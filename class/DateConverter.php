<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DateConverter
 *
 * @author marcos
 */
class DateConverter {

    public static function toDataBase($dataForm) {
        $cls_date = new DateTime(str_replace('/', '-', $dataForm));
        return $cls_date->format('Y-m-d');
    }

    public static function toForm($dataDB) {
        $newDate = $dataDB;
        return date('d/m/Y', strtotime($newDate)); //$dataDB;//$dataDB->format('d-m-Y');
    }

    public static function toFormDT($dataDB) {
        $newDate = $dataDB;
        return date('d/m/Y H:i:s', strtotime($newDate)); //$dataDB;//$dataDB->format('d-m-Y');
    }

    public static function toFormTime($dataDB) {
        $newDate = $dataDB;
        return date('H:i', strtotime($newDate)); //$dataDB;//$dataDB->format('d-m-Y');
    }

    public static function toDBDateTime($dataDB) {
        $newDate = $dataDB;
        return date('Y-m-d H:i:s', strtotime($newDate)); //$dataDB;//$dataDB->format('d-m-Y');
    }

    public static function dateToMysql($date) {
        $ano = substr($date, 6);
        $mes = substr($date, 3, -5);
        $dia = substr($date, 0, -8);
        return $ano . "-" . $mes . "-" . $dia;
    }

    public static function dateToMysqlDT($date) {
        $ano = substr($date, 6, -9);
        $mes = substr($date, 3, -14);
        $dia = substr($date, 0, -17);
        
        $hora = substr($date, 11, -6);
        $min = substr($date, 14, -3);
        $seg = substr($date, 17);
        return $ano . "-" . $mes . "-" . $dia. " ". $hora . ":" . $min . ":" . $seg;
    }

}
