<?php

class ErrorHandler {

    public static function printErrorForm($arrayErrors) {
        if (isset($arrayErrors)) {
            foreach ($arrayErrors as $valueMsgTab) {
                
                switch ($valueMsgTab["status"]) {
                    case "success":
                        ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo $valueMsgTab["msg"]; ?>
                        </div>
                        <?php
                        break;
                    case "info":
                        ?>
                        <div class="alert alert-info" role="alert">
                            <?php echo $valueMsgTab["msg"]; ?>
                        </div>
                        <?php 
                        break;
                    case "warning":
                        ?>
                        <div class="alert alert-warning" role="alert">
                            <?php echo $valueMsgTab["msg"]; ?>
                        </div>
                        <?php
                        break;
                    case "danger":
                        ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $valueMsgTab["msg"]; ?>
                        </div>
                        <?php
                        break;
                }
            }
        }
    }

}
?>
