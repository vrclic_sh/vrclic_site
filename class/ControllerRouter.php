<?php

class ControllerRouter {

    public function startRouter() {
        $app = new \Slim\Slim();

        $empresa = new Empresa();
        $empresa->loadDefault();

        //Home
        $app->get('/', function() use ($empresa) {
            //var_dump($empresa);
            require 'views/site/layout/index.php';
            exit();
        });

        $app->get('/img/:args+', function($args) use ($empresa) {
            header('Content-Type: image/jpeg');
            $token = $args[0];
            $width = $args[1];
            $typeImg = "1"; //JGEP
            if (count($args) >= 4) {
                $typeImg = $args[3];
            }
            $imgValt = new ImagemValt();
            $imgValt->loadByToken($token);
            $image = imagecreatefromstring(base64_decode($imgValt->getfile64()));

            $orig_width = imagesx($image);
            $orig_height = imagesy($image);
            if ($width > 0) {
                $height = (($orig_height * $width) / $orig_width);
                $new_image = imagecreatetruecolor($width, $height);
                imagecopyresized($new_image, $image,
                        0, 0, 0, 0,
                        $width, $height,
                        $orig_width, $orig_height);
                if ($typeImg == "1") {
                    imagejpeg($new_image);
                } else if ($typeImg == "2") {
                    imagepng($new_image);
                } else {
                    imagepng($new_image);
                }
            } else {
                if ($typeImg == "1") {
                    imagejpeg($image);
                } else if ($typeImg == "2") {
                    imagepng($image);
                } else {
                    imagepng($image);
                }
            }
            exit();
        });

        $app->get('/imgnews/:args+', function($args) use ($empresa) {
            header('Content-Type: image/jpeg');
            $token = $args[0];
            $width = $args[1];
            $typeImg = "1"; //JGEP
            if (count($args) >= 4) {
                $typeImg = $args[3];
            }
            $img = new MidiaNoticia();
            $img->loadByToken($token);

            if ($img->getrec_type() == 0) {
                $image = imagecreatefromstring(base64_decode($img->getfile64()));
            }else{
                $image = imagecreatefromjpeg($img->getpatch().$img->getnome_img());
            }
            $orig_width = imagesx($image);
            $orig_height = imagesy($image);
            if ($width > 0) {
                $height = (($orig_height * $width) / $orig_width);
                $new_image = imagecreatetruecolor($width, $height);
                imagecopyresized($new_image, $image,
                        0, 0, 0, 0,
                        $width, $height,
                        $orig_width, $orig_height);
                if ($typeImg == "1") {
                    imagejpeg($new_image);
                } else if ($typeImg == "2") {
                    imagepng($new_image);
                } else {
                    imagepng($new_image);
                }
            } else {
                if ($typeImg == "1") {
                    imagejpeg($image);
                } else if ($typeImg == "2") {
                    imagepng($image);
                } else {
                    imagepng($image);
                }
            }
            exit();
        });

        $app->get('/imgeve/:args+', function($args) use ($empresa) {
            header('Content-Type: image/jpeg');
            $token = $args[0];
            $width = $args[1];
            $typeImg = "1"; //JGEP
            if (count($args) >= 4) {
                $typeImg = $args[3];
            }
            $img = new MidiaEvento();
            $img->loadByToken($token);

            if ($img->getrec_type() == 0) {
                $image = imagecreatefromstring(base64_decode($img->getfile64()));
            }else{
                $image = imagecreatefromjpeg($img->getpatch().$img->getnome_img());
            }
            $orig_width = imagesx($image);
            $orig_height = imagesy($image);
            if ($width > 0) {
                $height = (($orig_height * $width) / $orig_width);
                $new_image = imagecreatetruecolor($width, $height);
                imagecopyresized($new_image, $image,
                        0, 0, 0, 0,
                        $width, $height,
                        $orig_width, $orig_height);
                if ($typeImg == "1") {
                    imagejpeg($new_image);
                } else if ($typeImg == "2") {
                    imagepng($new_image);
                } else {
                    imagepng($new_image);
                }
            } else {
                if ($typeImg == "1") {
                    imagejpeg($image);
                } else if ($typeImg == "2") {
                    imagepng($image);
                } else {
                    imagepng($image);
                }
            }
            exit();
        });

        $app->get('/imgemp/:args+', function($args) use ($empresa) {
            header('Content-Type: image/jpeg');
            $token = $args[0];
            $width = $args[1];
            $typeImg = "1"; //JGEP
            if (count($args) >= 4) {
                $typeImg = $args[3];
            }
            $imgValt = new MidiaEmpresa();
            $imgValt->loadByToken($token);
            $image = imagecreatefromstring(base64_decode($imgValt->getfile64()));

            $orig_width = imagesx($image);
            $orig_height = imagesy($image);
            if ($width > 0) {
                $height = (($orig_height * $width) / $orig_width);
                $new_image = imagecreatetruecolor($width, $height);
                imagecopyresized($new_image, $image,
                        0, 0, 0, 0,
                        $width, $height,
                        $orig_width, $orig_height);
                if ($typeImg == "1") {
                    imagejpeg($new_image);
                } else if ($typeImg == "2") {
                    imagepng($new_image);
                } else {
                    imagepng($new_image);
                }
            } else {
                if ($typeImg == "1") {
                    imagejpeg($image);
                } else if ($typeImg == "2") {
                    imagepng($image);
                } else {
                    imagepng($image);
                }
            }
            exit();
        });
        
        $app->get('/jsonapi/:args+', function($args) use ($empresa) {
            header("Content-Type: application/json");
            $token = $args[1];
            $page = $args[2];
            $noticia = new Noticia();
            echo $noticia->getJsonPagInfinit($page);
            exit();
        });


        $app->get('/captcha', function() use ($empresa) {
            header("Content-type: image/jpeg");
            $im = imagecreate(100, 30);
            $white = imagecolorallocate($im, 255, 255, 255);
            $black = imagecolorallocate($im, 0, 0, 0);
            $word = substr(TokenManager::token_encode(), 1, 6);
            $_SESSION[$word] = $word;
            imagettftext($im, 18, 0, 1, 25, $black, "/res/site/layout/fonts/arial.ttf",
                    $word);
            imagejpeg($im);
            imagedestroy($im);
            exit();
        });

        //Rotas Dinamicas
        $app->get('/a/:args+', function($args) use ($empresa) {
            $typeReq = "GET";
            require 'views/site/layout/' . $args[0] . '.php';
            exit();
        });

        $app->post('/a/:args+', function($args) use ($empresa) {
            $typeReq = "POST";
            require 'views/site/layout/' . $args[0] . '.php';
            exit();
        });

        //ADMIN
        $app->get('/admin', $this->authenticateForRole(1), function ($pag = 0) use ($empresa) {
            $erros = array();
            $sucessos = array();
            require 'views/adm/index.php';
            exit();
        });

        $app->get('/admin/a/:args+', $this->authenticateForRole(1), function($args) use ($empresa) {
            $typeReq = "GET";
            require 'views/adm/' . $args[0] . '.php';
            exit();
        });

        $app->post('/admin/a/:args+', $this->authenticateForRole(1), function($args) use ($empresa) {
            $typeReq = "POST";
            require 'views/adm/' . $args[0] . '.php';
            exit();
        });


        //Sair
        $app->get('/sair', function () use ($empresa) {
            SessionManager::destroy();
            require 'views/site/layout/index.php';
            exit();
        });




        $app->run();
    }

    function authenticateForRole($role = 0) {
        return function () use ( $role ) {

//            if (SessionManager::isValid() && isset($_SESSION[Usuario::SESSION_NAME])) {
//                $id = (int) $_SESSION[Usuario::SESSION_NAME];
//                $usuario = new Usuario();
//                $usuario->loadById($id);
//                if (null === $usuario->getid()) {
//                    $app = \Slim\Slim::getInstance();
//                    $app->flash('error', 'Login required');
//                    $app->redirect('/a/login');
//                }
//            } else {
//                $app = \Slim\Slim::getInstance();
//                $app->flash('error', 'Login required');
//                $app->redirect('/a/login');
//            }
        };
    }

}
