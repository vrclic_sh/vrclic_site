<?php

class Link extends Model {

    public function __construct() {
        $this->TB_NAME = "tb_link";
    }

    public function getAll() {
        return $this->selectDB();
    }

    public function getListPag($pag = 0) {
        $results = array();
        $filds = array();
        $fildsWhere = array();
        $fildsOrder = array(
            ":titulo" => "ASC"
        );
        $results = $this->selectDB($filds, $fildsWhere, $fildsOrder, $pag);
        return $results;
    }

    public function insertForm() {
        $retornoMsg = array();
        $fildsValidacao = array("titulo", "link", "status");
        $filds = array(
            ":titulo" => null,
            ":link" => null,
            ":status" => null
        );

        $retornoMsg = $this->insertDB($filds, $fildsValidacao);
        return $retornoMsg;
    }

    public function updateForm($id) {
        $retornoMsg = array();
        $fildsValidacao = array("titulo", "link", "status");
        $filds = array(
            ":titulo" => null,
            ":link" => null,
            ":status" => null
        );
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->updateDB($filds, $fildsWhere, $fildsValidacao);
        return $retornoMsg;
    }

    public function deleteForm($id) {
        $retornoMsg = array();
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->deleteDB($fildsWhere);
        return $retornoMsg;
    }
}
?>