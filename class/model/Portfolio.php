<?php

class Portfolio extends Model {

    public function __construct() {
        $this->TB_NAME = "tb_portfolio";
    }

    public function getAll() {
        return $this->selectDB();
    }

    public function getListPag($pag = 0, $fildsPar = array()) {
        $results = array();
        $filds = array();
        if(count($fildsPar) > 0){
            $filds = $fildsPar;
        }
        $fildsWhere = array();
        $fildsOrder = array(
            ":data_criacao" => "DESC"
        );
        $results = $this->selectDB($filds, $fildsWhere, $fildsOrder, $pag);
        return $results;
    }

    public function insertForm() {
        $retornoMsg = array();
        $fildsValidacao = array("tb_grupo_portfolio_id", "nome");
        $filds = array(
            ":tb_grupo_portfolio_id" => null,
            ":nome" => null,
            ":link" => null,
            ":data_publicacao" => (isset($_POST["data_publicacao"])?DateConverter::dateToMysqlDT($_POST["data_publicacao"]):null),
            ":status" => null
        );
        if (FileUpload::fileExist($_FILES['imagem01frm'])) {
            $objetoLocal = new Portfolio();
            $token_img = TokenManager::token_encode();
            $imagemValt = new ImagemValt();
            $retornoMsg = $imagemValt->uploadToValt($_FILES['imagem01frm'], $token_img);
            $filds[":token_img"] = $token_img;
        }
        $retornoMsg = $this->insertDB($filds, $fildsValidacao);
        return $retornoMsg;
    }

    public function updateForm($id) {
        $retornoMsg = array();
        $fildsValidacao = array("tb_grupo_portfolio_id", "nome");
        $filds = array(
            ":tb_grupo_portfolio_id" => null,
            ":nome" => null,
            ":link" => null,
            ":data_publicacao" => (isset($_POST["data_publicacao"])?DateConverter::dateToMysqlDT($_POST["data_publicacao"]):null),
            ":status" => null
        );
        $fildsWhere = array(
            ":id" => $id
        );
        if (FileUpload::fileExist($_FILES['imagem01frm'])) {
            $token_img = "";
            $objetoLocal = new Portfolio();
            $objetoLocal->loadById($id);
            if (null != $objetoLocal->gettoken_img() && !empty($objetoLocal->gettoken_img())) {
                $token_img = $objetoLocal->gettoken_img();
            } else {
                $token_img = TokenManager::token_encode();
            }
            $imagemValt = new ImagemValt();
            $retornoMsg = $imagemValt->uploadToValt($_FILES['imagem01frm'], $token_img);
            $filds[":token_img"] = $token_img;
        }
        $retornoMsg = $this->updateDB($filds, $fildsWhere, $fildsValidacao);
        return $retornoMsg;
    }

    public function deleteForm($id) {
        $retornoMsg = array();
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->deleteDB($fildsWhere);
        return $retornoMsg;
    }
    
    public function getJsonPagInfinit($pag = 0){
        $returnAr = array();
        $returnAr = $this->getListPag($pag, array("id", "data_criacao", "nome", "link", "token_img"));
        return json_encode($returnAr);
    }
           
}
?>