<?php

class MidiaNoticia extends Model {

    public function __construct() {
        $this->TB_NAME = "tb_midia_noticia";
    }

    public function getAll() {
        return $this->selectDB();
    }

    public function getListPag($pag = 0, $codigoS) {
        $results = array();
        $filds = array();
        $fildsWhere = array(
            ":tb_noticia_id" => $codigoS,
        );
        $fildsOrder = array();
        $results = $this->selectDB($filds, $fildsWhere, $fildsOrder, $pag);
        return $results;
    }
    
    public function getListNoticia($codigoNoticia) {
        $results = array();
        $filds = array("id", "token", "data_criacao");
        $fildsWhere = array(
            ":tb_noticia_id" => $codigoNoticia,
        );
        $results = $this->selectDB($filds, $fildsWhere);
        return $results;
    }
    
    public function loadByToken($token) {
        $results = $this->selectDB(array(), array(":token" => $token));
        if (count($results) > 0) {
            $this->setValues($results[0]);
        }
    }

    public function insertForm($codNoticia, $files) {
        $retornoMsg = array();
        $retornoInsert = FileUpload::uploadFilesMulti($files, 1);
        foreach ($retornoInsert as $value) {
            $filds = array(
                ":tb_noticia_id" => $codNoticia,
                ":rec_type" => 1
            );
            //var_dump(count($value["erro"][0]));
            if(count($value["erro"][0]) < 1){
                $filds[":nome_img"] = $value["filds"][":nome_img"];
                $filds[":type_img"] = $value["filds"][":type_img"];
                $filds[":file64"] = $value["filds"][":file64"];
                $filds[":patch"] = $value["filds"][":patch"];
                $filds[":nome_img"] = $value["filds"][":nome_img"];
                $filds[":token"] = TokenManager::token_encode();
                $retornoMsg = $this->insertDB($filds);
                
            }else{
                $retornoMsg[] = $value["erro"][0];
            }

        }
        
        return $retornoMsg;
    }

    public function updateForm($id) {
        $retornoMsg = array();
        $fildsValidacao = array("status", "destaque", "titulo");
        $filds = array(
            ":tb_categoria_id" => null,
            ":status" => null,
            ":destaque" => null,
            ":ordem" => null,
            ":titulo" => null,
            ":cor" => null
        );
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->updateDB($filds, $fildsWhere, $fildsValidacao);
        return $retornoMsg;
    }

    public function deleteForm($id) {
        $retornoMsg = array();
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->deleteDB($fildsWhere);
        return $retornoMsg;
    }

}

?>