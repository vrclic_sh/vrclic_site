<?php

class Usuario extends Model {

    const SESSION_NAME = "idUsuario";
    
    public function __construct() {
        $this->TB_NAME = "tb_usuario";
    }

    public function getAll() {
        return $this->selectDB();
    }
    
    public function login($email, $senha) {
        $filds = array(
            "id"
        );
        $fildsWhere = array(
            ":status" => 1,
            ":email" => $email,
            ":senha" => TokenManager::passwordTransform($senha)
        );
        $result = $this->selectDB($filds, $fildsWhere);
        if (count($result) > 0) {
            $_SESSION[Usuario::SESSION_NAME] = $result[0]["id"];
            $_SESSION['LAST_ACTIVITY'] = time();
            //$this->loadById($result[0]["id"]);
            return true;
        } else {
            $_SESSION[Usuario::SESSION_NAME] = null;
            return false;
        }
    }
    
    public static function getLogadoLog(){
        $usuario = Usuario::getLogado();
        return $usuario->getid() ." - ". $usuario->getnome();
    }
        
    public static function getLogado() {
        $id = (int) $_SESSION[Usuario::SESSION_NAME];
        if (is_numeric($id)) {
            $usuario = new Usuario();
            $usuario->loadById($id);
            return $usuario;
        } else {
            header("Location: /login");
        }
    }
    
    public function getListPag($pag = 0) {
        $results = array();
        $filds = array();
        $fildsWhere = array();
        $fildsOrder = array(
            ":nome" => "DESC"
        );
        $results = $this->selectDB($filds, $fildsWhere, $fildsOrder, $pag);
        return $results;
    }


    public function getformCreate() {
        $frm = '
            <div class="box-body">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Enter nome"/>
                </div>
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email"/>
                </div>
                <div class="form-group">
                    <label for="senha1">Senha</label>
                    <input type="password" class="form-control" id="senha1" name="senha1" placeholder="Senha">
                </div>
                <div class="form-group">
                    <label for="senha2">Repita a senha</label>
                    <input type="password" class="form-control" id="senha2" name="senha2" placeholder="Repita a senha">
                </div>
                <div class="form-group">
                    <label for="status">STATUS</label>
                    <select class="form-control" id="status" name="status">
                        <option value="1">Ativo</option>
                        <option value="0">Inativo</option>
                    </select>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-address-card"></i> SALVAR</button>
            </div>
            ';
        echo $frm;
    }

    public function getformAlter($value) {
        $frm = '
            <div class="box-body">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Enter nome" value="' . $value["nome"] . '"/>
                </div>
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="email" class="form-control" disabled id="email" name="email" placeholder="Enter email" value="' . $value["email"] . '"/>
                </div>
                <div class="form-group">
                    <label for="senha1">Senha</label>
                    <input type="password" class="form-control" id="senha1" name="senha1" placeholder="Senha">
                </div>
                <div class="form-group">
                    <label for="senha2">Repita a senha</label>
                    <input type="password" class="form-control" id="senha2" name="senha2" placeholder="Repita a senha">
                </div>
                <div class="form-group">
                    <label for="status">STATUS</label>
                    <select class="form-control" id="status" name="status">
                        <option value="1" ' . ($value["status"] == 1 ? "selected" : "") . '>Ativo</option>
                        <option value="0" ' . ($value["status"] == 0 ? "selected" : "") . '>Inativo</option>
                    </select>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> SALVAR ALTERAÇÕES</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> CANCELAR</button>
            </div>
            ';
        echo $frm;
    }

    public function getformDel($value) {
        $frm = '
            <div class="box-body">
                <div class="form-group">
                    <label for="nome">Nome: </label>
                    ' . $value["nome"] . '
                </div>
                <div class="form-group">
                    <label for="email">E-Mail: </label>
                    ' . $value["email"] . '
                </div>                                                                     
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> EXCLUIR</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> CANCELAR</button>
            </div>
            ';
        echo $frm;
    }

    public function insertForm() {
        $retornoMsg = array();
        $filds = array(
            ":tb_categoria_id" => $this->gettb_categoria_id(),
            ":status" => $this->getstatus(),
            ":destaque" => $this->getdestaque(),
            ":ordem" => $this->getordem(),
            ":titulo" => $this->gettitulo(),
            ":cor" => $this->getcor()
        );
        $retornoMsg = $this->insertDB($filds);
        return $retornoMsg;
    }

    public function updateForm($id) {
        $retornoMsg = array();
        $filds = array(
            ":status" => $this->getstatus(),
            ":destaque" => $this->getdestaque(),
            ":ordem" => $this->getordem(),
            ":titulo" => $this->gettitulo(),
            ":cor" => $this->getcor()
        );
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->updateDB($filds, $fildsWhere);
        return $retornoMsg;
    }

    public function deleteForm($id) {
        $retornoMsg = array();
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->deleteDB($fildsWhere);
        return $retornoMsg;
    }

}

?>