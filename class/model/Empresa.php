<?php

class Empresa extends Model {

    public function __construct() {
        $this->TB_NAME = "tb_empresa";
    }

    public function getAll() {
        return $this->selectDB();
    }
    
    public function loadDefault() {
        $results = $this->selectDB();
        if (count($results) > 0) {
            $this->setValues($results[0]);
        }
    }

    public function getformAlter($value) {
        $frm = '
            <div class="box-body">
                <div class="panel panel-dark">
                    <div class="panel-heading"><b>A Empresa</b></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="nome">Nome da Empresa</label>
                            <input type="text" class="form-control" id="nome" name="nome"  placeholder="Nome" value="' . $this->getnome() . '"/>
                        </div> 
                        <div class="form-group">
                            <label for="pagina_titulo">Titulo da página</label>
                            <input type="text" class="form-control" id="pagina_titulo" name="pagina_titulo"  placeholder="Titulo da página" value="' . $this->getpagina_titulo() . '"/>
                        </div>
                        <div class="form-group">
                            <label for="pagina_keywords">Palavras Chaves</label>
                            <input type="text" class="form-control" id="pagina_keywords" name="pagina_keywords"  placeholder="Início das Inscrições" value="' . $this->getpagina_keywords() . '"/>
                        </div>
                        <div class="form-group">
                            <label for="conteudo">Conteúdo</label>
                            <textarea name="conteudo" placeholder="Conteúdo" class="form-control summernote">' . $this->getconteudo() . '</textarea>
                        </div>
                    </div>
                </div>

                <div class="panel panel-dark">
                    <div class="panel-heading"><b>Configurações de Pagamento - PAGSEGURO</b></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="token_ps">Chave / TOKEN</label>
                            <input type="text" class="form-control" id="token_ps" name="token_ps" placeholder="Chave/Token" value="' . $this->gettoken_ps() . '">
                        </div>
                        <div class="form-group">
                            <label for="email_ps">E-mail da Conta</label>
                            <input type="text" class="form-control" id="email_ps" name="email_ps" placeholder="E-mail da Conta" value="' . $this->getemail_ps() . '">
                        </div>                                        
                    </div>
                </div>

                <div class="panel panel-dark">
                    <div class="panel-heading"><b>Contato Empresa</b></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="contato_email">Email para Contato</label>
                            <input type="text" class="form-control" id="contato_email" name="contato_email" placeholder="Email para Contato" value="' . $this->getcontato_email() . '">
                        </div>
                        <div class="form-group">
                            <label for="contato_fone">Telefone para Contato</label>
                            <input type="text" class="form-control" id="contato_fone" name="contato_fone" placeholder="Telefone para Contato" value="' . $this->getcontato_fone() . '">
                        </div>
                        <div class="form-group">
                            <label for="contato_cel">Celular para Contato</label>
                            <input type="text" class="form-control" id="contato_cel" name="contato_cel" placeholder="Celular para Contato" value="' . $this->getcontato_cel() . '">
                        </div>
                        <div class="form-group">
                            <label for="contato_whatsapp">Celular para WhatsApp</label>
                            <input type="text" class="form-control" id="contato_whatsapp" name="contato_whatsapp" placeholder="Se informar, pessoas podem iníciar uma conversa diretamente pelo site." value="' . $this->getcontato_whatsapp() . '">
                        </div>
                        <div class="form-group">
                            <label for="contato_endereco">Endereço para Contato</label>
                            <input type="text" class="form-control" id="contato_endereco" name="contato_endereco" placeholder="Endereço para Contato" value="' . $this->getcontato_endereco() . '">
                        </div>
                        <div class="form-group">
                            <label for="contato_cidade">Cidade para Contato</label>
                            <input type="text" class="form-control" id="contato_cidade" name="contato_cidade" placeholder="Cidade para Contato" value="' . $this->getcontato_cidade() . '">
                        </div>
                        <div class="form-group">
                            <label for="contato_estado">Estado para Contato</label>
                            <input type="text" class="form-control" id="contato_estado" name="contato_estado" placeholder="Estado para Contato" value="' . $this->getcontato_estado() . '">
                        </div>
                        <div class="form-group">
                            <label for="contato_maps">Google Maps</label>
                            <input type="text" class="form-control" id="contato_maps" name="contato_maps" placeholder="Google Maps" value="' . $this->getcontato_maps() . '">
                        </div>
                    </div>
                </div>
                <div class="panel panel-dark">
                    <div class="panel-heading"><b>Links da Empresa</b></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="media_facebook">Link Facebook</label>
                            <input type="text" class="form-control" id="media_facebook" name="media_facebook" placeholder="Link Facebook" value="' . $this->getmedia_facebook() . '">
                        </div>
                        <div class="form-group">
                            <label for="media_insta">Link Instagram</label>
                            <input type="text" class="form-control" id="media_insta" name="media_insta" placeholder="Link Instagram" value="' . $this->getmedia_insta() . '">
                        </div>
                        <div class="form-group">
                            <label for="media_linkedin">Link para Linkedin</label>
                            <input type="text" class="form-control" id="media_linkedin" name="media_linkedin" placeholder="Link para Llinkedin" value="' . $this->getmedia_linkedin() . '">
                        </div>
                        <div class="form-group">
                            <label for="media_twitter">Link Twitter</label>
                            <input type="text" class="form-control" id="media_twitter" name="media_twitter" placeholder="Link Twitter" value="' . $this->getmedia_twitter() . '">
                        </div>                                        
                        <div class="form-group">
                            <label for="link_web_email">Link Web Mail</label>
                            <input type="text" class="form-control" id="link_web_email" name="link_web_email" placeholder="Link Web Mail" value="' . $this->getlink_web_email() . '">
                        </div>
                    </div>
                </div>
                <div class="panel panel-dark">
                    <div class="panel-heading"><b>Logos da Empresa</b></div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="token_logo_grande1">Logo Grande 01</label>
                                <img src="/img/' . $this->gettoken_logo_grande() . '/600/1/2"  class="img-responsive" alt=""/>
                            </div>
                            <div class="form-group">
                                <label for="imagem01frm">Logo Grande 01</label>
                                <input type="file" name="imagem01frm" id="imagem01frm" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="logo_icoma">Logo Grande 02</label>
                                <img src="/img/' . $this->gettoken_logo_pequena() . '/200/1/2"  class="img-responsive" alt=""/>
                            </div>
                            <div class="form-group">
                                <label for="imagem02frm">Logo Grande 02</label>
                                <input type="file" name="imagem02frm" id="imagem02frm" class="form-control"/>
                            </div>
                        </div>                                    
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <input type="submit" class="btn btn-success" value="Salvar Informações da Empresa"/>
            </div>
            ';
        echo $frm;
    }

    public function getformDel($value) {
        $frm = '
            <div class="box-body">
                <div class="form-group">
                    <label for="titulo">Título: </label>
                    ' . $value["titulo"] . '
                </div>                                                                       
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> EXCLUIR</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> CANCELAR</button>
            </div>
            ';
        echo $frm;
    }

    public function updateForm($id) {
        $retornoMsg = array();
        $filds = array(
                ":nome" => $this->getnome(),
                ":pagina_titulo" => $this->getpagina_titulo(),
                ":pagina_keywords" => $this->getpagina_keywords(),
                ":contato_email" => $this->getcontato_email(),
                ":contato_fone" => $this->getcontato_fone(),
                ":contato_cel" => $this->getcontato_cel(),
                ":contato_endereco" => $this->getcontato_endereco(),
                ":contato_cidade" => $this->getcontato_cidade(),
                ":contato_estado" => $this->getcontato_estado(),
                ":contato_maps" => $this->getcontato_maps(),
                ":media_facebook" => $this->getmedia_facebook(),
                ":media_insta" => $this->getmedia_insta(),
                ":media_linkedin" => $this->getmedia_linkedin(),
                ":media_twitter" => $this->getmedia_twitter(),
                ":link_web_email" => $this->getlink_web_email(),
                ":token_logo_grande" => $this->gettoken_logo_grande(),
                ":token_logo_pequena" => $this->gettoken_logo_pequena(),
                ":token_ps" => $this->gettoken_ps(),
                ":email_ps" => $this->getemail_ps(),
                ":conteudo" => $this->getconteudo(),
                ":contato_whatsapp" => $this->getcontato_whatsapp(),
                ":data_alteracao" => date("Y-m-d H:i:s")
            );
            if (FileUpload::fileExist($_FILES['imagem01frm'])) {
                $token_img = TokenManager::token_encode();
                $imagemValt = new ImagemValt();
                $imagemValt->inserir64($_FILES['imagem01frm'], $token_img);
                $filds[":token_logo_grande"] = $token_img;
            }
            if (FileUpload::fileExist($_FILES['imagem02frm'])) {
                $token_img = TokenManager::token_encode();
                $imagemValt = new ImagemValt();
                $imagemValt->inserir64($_FILES['imagem02frm'], $token_img);
                $filds[":token_logo_pequena"] = $token_img;
            }
            $fildsWhere = array(
                ":id" => $id
            );
        $retornoMsg = $this->updateDB($filds, $fildsWhere);
        return $retornoMsg;
    }

    public function deleteForm($id) {
        $retornoMsg = array();
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->deleteDB($fildsWhere);
        return $retornoMsg;
    }

}

?>