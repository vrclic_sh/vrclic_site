<?php

class Noticia extends Model {

    public function __construct() {
        $this->TB_NAME = "tb_noticia";
    }

    public function getAll() {
        return $this->selectDB();
    }

    public function getListPag($pag = 0, $fildsPar = array()) {
        $results = array();
        $filds = array();
        if(count($fildsPar) > 0){
            $filds = $fildsPar;
        }
        $fildsWhere = array();
        $fildsOrder = array(
            ":data_criacao" => "DESC"
        );
        $results = $this->selectDB($filds, $fildsWhere, $fildsOrder, $pag);
        return $results;
    }

    public function insertForm() {
        $retornoMsg = array();
        $fildsValidacao = array("tb_categoria_id", "status", "titulo");
        $filds = array(
            ":tb_categoria_id" => null,
            ":status" => null,
            ":data_ini" => (isset($_POST["data_ini"])?DateConverter::dateToMysqlDT($_POST["data_ini"]):null),
            ":data_fim" => (isset($_POST["data_fim"])?DateConverter::dateToMysqlDT($_POST["data_fim"]):null),
            ":data_ini_destaque" => (isset($_POST["data_ini_destaque"])?DateConverter::dateToMysqlDT($_POST["data_ini_destaque"]):null),
            ":data_fim_destaque" => (isset($_POST["data_fim_destaque"])?DateConverter::dateToMysqlDT($_POST["data_fim_destaque"]):null),
            ":destaque_posicao" => null,
            ":titulo" => null,
            ":conteudo" => null
        );
        if (FileUpload::fileExist($_FILES['imagem01frm'])) {
            $objetoLocal = new Anuncio();
            $token_img = TokenManager::token_encode();
            $imagemValt = new ImagemValt();
            $retornoMsg = $imagemValt->uploadToValt($_FILES['imagem01frm'], $token_img);
            $filds[":token_img"] = $token_img;
        }
        $retornoMsg = $this->insertDB($filds, $fildsValidacao);
        return $retornoMsg;
    }

    public function updateForm($id) {
        $retornoMsg = array();
        $fildsValidacao = array("tb_categoria_id", "status", "titulo");
        $filds = array(
            ":tb_categoria_id" => null,
            ":status" => null,
            ":data_ini" => (isset($_POST["data_ini"])?DateConverter::dateToMysqlDT($_POST["data_ini"]):null),
            ":data_fim" => (isset($_POST["data_fim"])?DateConverter::dateToMysqlDT($_POST["data_fim"]):null),
            ":data_ini_destaque" => (isset($_POST["data_ini_destaque"])?DateConverter::dateToMysqlDT($_POST["data_ini_destaque"]):null),
            ":data_fim_destaque" => (isset($_POST["data_fim_destaque"])?DateConverter::dateToMysqlDT($_POST["data_fim_destaque"]):null),
            ":destaque_posicao" => null,
            ":titulo" => null,
            ":conteudo" => null
        );
        $fildsWhere = array(
            ":id" => $id
        );
        if (FileUpload::fileExist($_FILES['imagem01frm'])) {
            $token_img = "";
            $objetoLocal = new Noticia();
            $objetoLocal->loadById($id);
            if (null != $objetoLocal->gettoken_img() && !empty($objetoLocal->gettoken_img())) {
                $token_img = $objetoLocal->gettoken_img();
            } else {
                $token_img = TokenManager::token_encode();
            }
            $imagemValt = new ImagemValt();
            $retornoMsg = $imagemValt->uploadToValt($_FILES['imagem01frm'], $token_img);
            $filds[":token_img"] = $token_img;
        }
        $retornoMsg = $this->updateDB($filds, $fildsWhere, $fildsValidacao);
        return $retornoMsg;
    }

    public function deleteForm($id) {
        $retornoMsg = array();
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->deleteDB($fildsWhere);
        return $retornoMsg;
    }
    
    public function loadByToken($token) {
        $fildsWhere = array(
            ":token" => $token,
            ":deleted" => 0
        );
        $results = $this->selectDB(array(), $fildsWhere);
        if (count($results) > 0) {
            $this->setValues($results[0]);
        }
    }
    
    public function getJsonPagInfinit($pag = 0){
        $returnAr = array();
        $returnAr = $this->getListPag($pag, array("id", "data_criacao", "titulo", "conteudo", "token_img"));
        return json_encode($returnAr);
    }
           
}
?>