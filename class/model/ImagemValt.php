<?php

class ImagemValt extends Model {

    public function __construct() {
        $this->TB_NAME = "tb_imagem_valt";
    }

    public function getAll() {
        return $this->selectDB();
    }

    public function getListPag($pag = 0) {
        $results = array();
        $filds = array();
        $fildsWhere = array();
        $fildsOrder = array();
        $results = $this->selectDB($filds, $fildsWhere, $fildsOrder, $pag);
        return $results;
    }

    public function loadByToken($token) {
        $results = $this->selectDB(array(), array(":token" => $token));
        if (count($results) > 0) {
            $this->setValues($results[0]);
        }
    }
    
    public function existByToken($token) {
        $results = $this->selectDB(array("token"), array(":token" => $token));
        if (count($results) > 0) {
            return true;
        }else{
            return false;
        }
    }

    public function deleteForm($id) {
        $retornoMsg = array();
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->deleteDB($fildsWhere);
        return $retornoMsg;
    }

    public static function uploadToValt($file, $token, $type = 0) {
        $retornoMsg = array();
        $update = 0;
        $filds = array(
            ":nome_img" => $file["name"],
            ":type_img" => $file["type"],
            ":rec_type" => $type
        );
        if (FileUpload::fileExist($file)) {
            $retornoMsg = FileUpload::validaTipoImagem($file);
            if (count($retornoMsg) > 0) {
                return $retornoMsg;
            }
            $imagemValt = new ImagemValt();
            if($imagemValt->existByToken($token)){
                $update = 1;
            }
            if ($type == 0) {
                $filds[":file64"] = FileUpload::toBase64($file);
            } else {
                $filds[":patch"] = "";
            }
            if ($update == 0) {
                $filds[":token"] = $token;
                $retornoMsg = $imagemValt->insertDB($filds);
            } else {
                $fildsWhere = array(
                    ":token" => $token
                );
                $retornoMsg = $imagemValt->updateDB($filds, $fildsWhere);
            }
        }
        return $retornoMsg;
    }

}

?>