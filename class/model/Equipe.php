<?php

class Equipe extends Model {

    public function __construct() {
        $this->TB_NAME = "tb_equipe";
    }

    public function getAll() { 
        return $this->selectDB();
    }

    public function getListPag($pag = 0) {
        $results = array();
        $filds = array();
        $fildsWhere = array();
        $fildsOrder = array(
            ":nome" => "ASC"
        );
        $results = $this->selectDB($filds, $fildsWhere, $fildsOrder, $pag);
        return $results;
    }

    public function insertForm() {
        $retornoMsg = array();
        $fildsValidacao = array("nome", "funcao");
        $filds = array(
            ":status" => null,
            ":nome" => null,
            ":sexo" => null,
            ":telefone" => null,
            ":celular" => null,
            ":email" => null,
            ":facebook" => null,
            ":instagram" => null,
            ":linkedin" => null,
            ":site" => null,
            ":twitter" => null,
            ":youtube" => null,
            ":twitch" => null,
            ":tinder" => null,
            ":whatsapp" => null,
            ":funcao" => null,
            ":descricao" => null
        );
        if (FileUpload::fileExist($_FILES['imagem01frm'])) {
            $objetoLocal = new Equipe();
            $token_img = TokenManager::token_encode();
            $imagemValt = new ImagemValt();
            $retornoMsg = $imagemValt->uploadToValt($_FILES['imagem01frm'], $token_img);
            $filds[":token_img"] = $token_img;
        }
        $retornoMsg = $this->insertDB($filds, $fildsValidacao);
        return $retornoMsg;
    }

    public function updateForm($id) {
        $retornoMsg = array();
        $fildsValidacao = array("nome", "funcao");
        $filds = array(
            ":status" => null,
            ":nome" => null,
            ":sexo" => null,
            ":telefone" => null,
            ":celular" => null,
            ":email" => null,
            ":facebook" => null,
            ":instagram" => null,
            ":linkedin" => null,
            ":site" => null,
            ":twitter" => null,
            ":youtube" => null,
            ":twitch" => null,
            ":tinder" => null,
            ":whatsapp" => null,
            ":funcao" => null,
            ":descricao" => null
        );
        $fildsWhere = array(
            ":id" => $id
        );
        if (FileUpload::fileExist($_FILES['imagem01frm'])) {
            $token_img = "";
            $objetoLocal = new Equipe();
            $objetoLocal->loadById($id);
            if (null != $objetoLocal->gettoken_img() && !empty($objetoLocal->gettoken_img())) {
                $token_img = $objetoLocal->gettoken_img();
            } else {
                $token_img = TokenManager::token_encode();
            }
            $imagemValt = new ImagemValt();
            $retornoMsg = $imagemValt->uploadToValt($_FILES['imagem01frm'], $token_img);
            $filds[":token_img"] = $token_img;
        }
        $retornoMsg = $this->updateDB($filds, $fildsWhere, $fildsValidacao);
        return $retornoMsg;
    }

    public function deleteForm($id) {
        $retornoMsg = array();
        $fildsWhere = array(
            ":id" => $id
        );
        $retornoMsg = $this->deleteDB($fildsWhere);
        return $retornoMsg;
    }
}
?>