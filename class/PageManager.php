<?php

use Rain\Tpl;

class PageManager {

    private $tpl;
    private $options = [];
    private $defaults = [
        "data" => []
    ];

    

    private function setData($data = array()) {
        foreach ($data as $key => $value) {
            $this->tpl->assign($key, $value);
        }
    }

    public function setTpl($name, $data = array(), $returnHTML = false) {

        $this->setData($data);
        $this->tpl->draw($name, $returnHTML);
    }
    
    public function __construct($opts = array(), $viewPath = "view") {
        $this->options = array_merge($this->defaults, $opts);
        //echo $_SERVER["DOCUMENT_ROOT"];
        $config = array(
            "tpl_dir" => $_SERVER["DOCUMENT_ROOT"] . $viewPath,
            "cache_dir" => $_SERVER["DOCUMENT_ROOT"] . "/views-cache/",
            "debug" => false // set to false to improve the speed
        );

        Tpl::configure($config);

        $this->tpl = new Tpl;

        $this->setData($this->options["data"]);

        $this->tpl->draw("header");
    }

    public function __destruct() {
        $this->tpl->draw("footer");
    }

}
