<?php

class SitePageManager extends PageManager{
    
    public function __construct($opts = array(), $viewPath = "/views/site/") {
        parent::__construct($opts, $viewPath);
    }
    
}
