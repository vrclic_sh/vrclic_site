<?php

class SessionManager {

    public function __construct() {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    public function setSession($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function getSession($key) {
        
        return $_SESSION[$key];
    }

    public function isLogado() {
        return isset($_SESSION["usuarioId"]);
    }

    public function deleteSession($key) {
        session_unset($key);
    }

    public static function destroy() {
        session_destroy();
    }

    public static function isValid() {
        if (isset($_SESSION['LAST_ACTIVITY']) && ((time() - $_SESSION['LAST_ACTIVITY']) > 1800)) {
            // last request was more than 30 minutes ago
            session_unset();     // unset $_SESSION variable for the run-time 
            session_destroy();   // destroy session data in storage
            return false;
        } else {
            $_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
            return true;
        }
    }
    
    public static function getParametro($nome, $valor){
        if(!is_null($valor)){
             $_SESSION[$nome] = $valor;
             return $valor;
        }else{
            return $_SESSION[$nome];
        }
    }

}

?>
