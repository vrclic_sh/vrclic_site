<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PagSeguroM
 *
 * @author marcos
 */
class PagSeguroM {

    private $email = "vandersonbbarbosa@hotmail.com";
    private $token_sandbox = "0C67013E153545A983D146CE6BE84E0F";
    private $token_oficial = "0C67013E153545A983D146CE6BE84E0F";
    private $url_retorno = "http://firis.vrclic.com/pagamentoauto";
    //URL OFICIAL
    //COMENTE AS 4 LINHAS ABAIXO E DESCOMENTE AS URLS DA SANDBOX PARA REALIZAR TESTES
//    private $url = "https://ws.pagseguro.uol.com.br/v2/checkout/";
//    private $url_redirect = "https://pagseguro.uol.com.br/v2/checkout/payment.html?code=";
//    private $url_notificacao = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/';
//    private $url_transactions = 'https://ws.pagseguro.uol.com.br/v2/transactions/';
    //
    ////URL SANDBOX
    private $url = "https://ws.sandbox.pagseguro.uol.com.br/v2/checkout/";
    private $url_redirect = "https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=";
    private $url_notificacao = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/';
    private $url_transactions = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/';
    private $email_token = ""; //NÃO MODIFICAR
    private $statusCode = array(0 => "Pendente",
        1 => "Aguardando pagamento",
        2 => "Em análise",
        3 => "Pago",
        4 => "Disponível",
        5 => "Em disputa",
        6 => "Devolvida",
        7 => "Cancelada");

    public function __construct() {
        $this->email_token = "?email=" . $this->email . "&token=" . $this->token_oficial;
        $this->url .= $this->email_token;
    }

    public static function pagamentoPed($pedido, $titulo, $empresa) {
        //$url = 'https://ws.pagseguro.uol.com.br/v2/checkout';

        $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout';

        $data['token'] = $empresa->gettoken_ps();//'0C67013E153545A983D146CE6BE84E0F'; //Token Vanderson TESTES        
        $data['email'] = $empresa->getemail_ps();//'vandersonbbarbosa@hotmail.com';
        $data['currency'] = 'BRL';

        $data['reference'] = "PED" . $pedido->getid();

        $countfor = 0;
        foreach ($pedido->getListFotos() as $valueFoto) {
            $countfor++;
            $data['itemId' . $countfor] = $pedido->getid();
            $data['itemQuantity' . $countfor] = $valueFoto["quantidade"];
            $data['itemDescription' . $countfor] = $titulo;
            $data['itemAmount' . $countfor] = money_format('%i', $valueFoto["val_unitario"]);
        }

        $entrega = $pedido->getEntrega();

        if (null != $entrega->getid() && $entrega->getvalor() > 0) {
            $countfor++;
            $data['itemId' . $countfor] = $entrega->getid();
            $data['itemQuantity' . $countfor] = 1;
            $data['itemDescription' . $countfor] = $entrega->gettitulo();
            $data['itemAmount' . $countfor] = money_format('%i', $entrega->getvalor());
        }

        $data = http_build_query($data);

        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $xml = curl_exec($curl);

        curl_close($curl);

        $xml = simplexml_load_string($xml);




        echo $xml->code;

        if (count($xml->error) > 0) {
            $return = 'Dados Inválidos ' . $xml->error->message;
            echo $return;
            exit;
        }

        if ($xml == 'Unauthorized') {
            $return = 'Não Autorizado';
            echo $return;
            exit;
        }

        $pedido->updatechave_pagamento($pedido->getid(), $xml->code, 0, "Aguardando");
        
        exit();
    }

    public static function pagamentoEvento($evento, $titulo, $empresa) {
        //$url = 'https://ws.pagseguro.uol.com.br/v2/checkout';
        $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout';

        $data['token'] = $empresa->gettoken_ps();//'0C67013E153545A983D146CE6BE84E0F'; //Token Vanderson TESTES        
        $data['email'] = $empresa->getemail_ps();//'vandersonbbarbosa@hotmail.com';
        $data['currency'] = 'BRL';
        $countfor = 0;

        $data['reference'] = "EVE" . $evento->getid();


        $data['itemId1'] = $evento->getid();
        $data['itemQuantity1'] = 1;
        $data['itemDescription1'] = $evento->gettitulo();
        $data['itemAmount1'] = money_format('%i', $evento->calcValorAtual());



        $data = http_build_query($data);

        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $xmlRet = curl_exec($curl);

        curl_close($curl);

        $xml = simplexml_load_string($xmlRet);
        echo $xml->code;

        if (count($xml->error) > 0) {
            $return = 'Dados Inválidos ' . $xml->error->message;
            echo $return;
            exit;
        }

        if ($xml == 'Unauthorized') {
            $return = 'Não Autorizado';
            echo $return;
            exit;
        }

        $evento->updatechave_pagamento($evento->getid(), $xml->code, 0, $xmlRet);
    }

    public function executeNotification($POST) {
        $url = $this->url_notificacao . $POST['notificationCode'] . $this->email_token;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $transaction = curl_exec($curl);
        if ($transaction == 'Unauthorized') {
            //TRANSAÇÃO NÃO AUTORIZADA

            exit;
        }
        curl_close($curl);
        $transaction_obj = simplexml_load_string($transaction);
        return $transaction_obj;
    }

    //Obtém o status de um pagamento com base no código do PagSeguro
    //Se o pagamento existir, retorna um código de 1 a 7
    //Se o pagamento não exitir, retorna NULL
    public function getStatusByCode($code) {
        $url = $this->url_transactions . $code . $this->email_token;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $transaction = curl_exec($curl);

        var_dump($transaction);

        exit();
        if ($transaction == 'Unauthorized') {
            return -1;
        } else {
            $transaction_obj = simplexml_load_string($transaction);

            if (count($transaction_obj->error) > 0) {
                //Insira seu código avisando que o sistema está com problemas
//                var_dump($transaction_obj);
                return -1;
            }
            if (isset($transaction_obj->status))
                return $transaction_obj->status;
            else
                return -1;
        }
    }

    public static function descStatus($status) {
        $return = "";
        $statusCode = array(0 => "Pendente",
            1 => "Aguardando pagamento",
            2 => "Em análise",
            3 => "Pago",
            4 => "Disponível",
            5 => "Em disputa",
            6 => "Devolvida",
            7 => "Cancelada");
        foreach ($statusCode as $key => $value) {
            if ($status == $key) {
                $return = $value;
            }
        }
        return $return;
    }

}

?>