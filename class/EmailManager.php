<?php

class EmailManager {

    public static function sendRecSenha($email, $senha) {
        $subject = "FOTO ÍRIS - Recuperar senha"; //Assunto?
        $address = array(); //Enviar para?
        $address[] = $email; //Enviar para?
//        $address[] = "marcos@vrclic.com.br"; //Enviar para?
        $body = "Prezado " . $email . ", sua nova senha é: " . $senha;
        $replyTo = "mail@vrclic.com.br"; //Retornar para?
        return EmailManager::sendEmail($subject, $address, $body, $replyTo);
    }

    public static function sendAlertNewSenha($email) {
        $subject = "PESCA - TROCA DE SENHA"; //Assunto?
        $address = array(); //Enviar para?
        $address[] = $email; //Enviar para?
//        $address[] = "marcos@vrclic.com.br"; //Enviar para?
        $body = "Prezado " . $email . ", Sua senha foi alterada, caso não tenha sido você que realizou a troca, recuperar a senha imediatamente ou entrar em contato conosco.";
        $replyTo = "mail@vrclic.com.br"; //Retornar para?
        return EmailManager::sendEmail($subject, $address, $body, $replyTo);
    }

    public static function sendError($error, $titulo) {
        if (DebugHandler::isDebuging()) {
            echo '<br/>';
            echo "sendError - Entrou";
            echo '<br/>';
        }
        $subject = "ERRO - " . $titulo; //Assunto?
        $address = array(); //Enviar para?
        $address[] = "vanderson@vrclic.com.br"; //Enviar para?
        $address[] = "marcos@vrclic.com.br"; //Enviar para?
        $body = "Segue erro: "
                . $error . " <<Fim"; //Conteudo do e-mail
        $replyTo = "mail@vrclic.com.br"; //Retornar para?
        //var_dump($error);
        if (DebugHandler::isDebuging()) {
            echo '<br/>';
            echo "sendError - Vai enviar";
            echo '<br/>';
        }
        return EmailManager::sendEmail($subject, $address, $body, $replyTo);
    }

    public static function sendBoleto($corpo, $titulo, $toEmail, $empresa) {
        $subject = $titulo; //Assunto?
        $address = array(); //Enviar para?
        //$address[] = "vanderson@vrclic.com.br"; //Enviar para?
        //$address[] = "marcos@vrclic.com.br"; //Enviar para?
        
        $address[] = $empresa->getcontato_email();
        $address[] = $toEmail;
        $body = $corpo; //Conteudo do e-mail
        $replyTo = "mail@vrclic.com.br"; //Retornar para?
        //var_dump($error);

        return EmailManager::sendEmail($subject, $address, $body, $replyTo);
    }

    public static function sendContato($contato) {
        $subject = "CONTATO - AB"; //Assunto?
        $address = array(); //Enviar para?
        $address[] = "vanderson@vrclic.com.br"; //Enviar para?
        $address[] = "marcos@vrclic.com.br"; //Enviar para?
        $body = "Novo contato de: "
                . $contato->getnome() . ", "
                . "Falar sobre: "
                . $contato->getconteudo(); //Conteudo do e-mail
        $replyTo = "mail@vrclic.com.br"; //Retornar para?
        return EmailManager::sendEmail($subject, $address, $body, $replyTo);
    }

    public static function sendEmail($subject, $address, $body, $replyTo) {

        if (DebugHandler::isDebuging()) {
            echo '<br/>';
            echo "sendEmail - Entrou";
            echo '<br/>';
        }
        
        
        //require '../vendor/autoload.php';


// Inicia a classe PHPMailer
        //$mail = new PHPMailer(true);
        $mail = new PHPMailer\PHPMailer\PHPMailer(true);

// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        $mail->IsSMTP(); // Define que a mensagem será SMTP
        if (DebugHandler::isDebuging()) {
            echo '<br/>';
            echo "sendEmail - Define método SMTP";
            echo '<br/>';
        }
        try {
            if (DebugHandler::isDebuging()) {
                echo '<br/>';
                echo "sendEmail - Entrou no Try";
                echo '<br/>';
            }
//            $mail->Host = 'smtp.gmail.com'; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
            $mail->SMTPAuth = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
//            $mail->Port = 587; //  Usar 587 porta SMTP
            $mail->SMTPSecure = 'ssl';
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = 465;
            $mail->Username = 'mail@vrclic.com.br'; // Usuário do servidor SMTP (endereço de email)
            $mail->Password = 'm4trix.!@#'; // Senha do servidor SMTP (senha do email usado)
//Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=    
            $mail->SetFrom('mail@vrclic.com.br', 'VRCLIC'); //Seu e-mail
//            $mail->AddReplyTo($replyTo, $replyTo); //Seu e-mail
            $mail->Subject = $subject; //Assunto do e-mail
//Define os destinatário(s)
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//            $mail->AddAddress($address, $address);
            foreach ($address as $value) {
                $mail->AddAddress($value, $value);
            }
//Campos abaixo são opcionais 
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddCC('destinarario@dominio.com.br', 'Destinatario'); // Copia
//$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); // Cópia Oculta
//$mail->AddAttachment('images/phpmailer.gif');      // Adicionar um anexo
//Define o corpo do email
            $mail->MsgHTML($body);

////Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
//$mail->MsgHTML(file_get_contents('arquivo.html'));

            $mail->Send();
            return TRUE;
//return "Mensagem enviada com sucesso</p>\n";
//caso apresente algum erro é apresentado abaixo com essa exceção.
        } catch (phpmailerException $e) {
            if (DebugHandler::isDebuging()) {
                echo '<br/>';
                echo "Erro ao enviar o e-mail: " . $e;
                echo '<br/>';
            }
//            return $e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
            return FALSE; //$e->errorMessage(); //Mensagem de erro costumizada do PHPMailer
        }
    }

}

?>
