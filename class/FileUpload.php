<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileUpload
 *
 * @author marcos
 */
class FileUpload {

    public function multiUpload() {
        if (isset($_POST['upload'])) {
            $albumNew = $_POST['idAlbum'];

            if (!file_exists('../album/' . $albumNew)) {
                $folder = mkdir('../album/' . $albumNew, 0777, true);
            }

            $folder = '../album/' . $albumNew;
            //INFO IMAGEM
            $file = $_FILES['img'];
            $numFile = count(array_filter($file['name']));

            //PASTA
            //$folder = '../album';
            //REQUISITOS
            $permite = array('image/jpg', 'image/jpeg', 'image/png');
            $maxSize = 1024 * 1024 * 5;

            //MENSAGENS
            $msg = array();
            $errorMsg = array(
                1 => 'O arquivo no upload é maior do que o limite definido em upload_max_filesize no php.ini.',
                2 => 'O arquivo ultrapassa o limite de tamanho em MAX_FILE_SIZE que foi especificado no formulário HTML',
                3 => 'o upload do arquivo foi feito parcialmente',
                4 => 'Não foi feito o upload do arquivo'
            );

            if ($numFile <= 0) {
                echo 'Selecione uma Imagem!';
            } else {
                for ($i = 0; $i < $numFile; $i++) {
                    $name = $file['name'][$i];
                    $type = $file['type'][$i];
                    $size = $file['size'][$i];
                    $error = $file['error'][$i];
                    $tmp = $file['tmp_name'][$i];

                    $extensao = @end(explode('.', $name));
                    $novoNome = $name;

                    if ($error != 0) {
                        echo $msg[] = '<div id="mensDiversas" class="label label-danger"><b>' . $name . ' :</b> ' . $errorMsg[$error] . '</div>';
                    } else if (!in_array($type, $permite)) {
                        echo $msg[] = '<div id="mensDiversas" class="label label-danger"><b>' . $name . ' :</b> Erro! tipo de Arquivo não suportado!</div>';
                    } else if ($size > $maxSize) {
                        echo $msg[] = '<div id="mensDiversas" class="label label-danger"><b>' . $name . ' :</b> Erro! imagem ultrapassa o limite de 5MB</div>';
                    } else {
                        if (move_uploaded_file($tmp, $folder . '/' . $novoNome)) {
                            $query = "INSERT INTO album_fotos (id_alpe_alft, foto_alft) VALUES ('$albumNew', '$novoNome')";
                            mysqli_query($a, $query) or die(mysqli_error($a));
                            echo '<meta http-equiv="refresh" content="0; url=' . $_SERVER ['REQUEST_URI'] . '&op=1"/>';
                        } else {
                            echo '<meta http-equiv="refresh" content="0; url=' . $_SERVER ['REQUEST_URI'] . '&op=2"/>';
                        }
                    }
                }
            }
        }
    }

    public static function uploadFilesMulti2($files) {
        $retList = array();
        $qtdeItens = count($files['name']);
        if ($qtdeItens) {
            for ($i = 0; $i < $qtdeItens; $i++) {
                $data = file_get_contents($files['tmp_name'][$i]);
                $base64 = base64_encode($data);
                array_push($retList, $base64);
            }
        }
        return $retList;
    }

    public static function uploadFilesMulti($files, $type) {
        
        $retorno = array();
        for ($i = 0; $i < count($files["tmp_name"]); $i++) {
            $retornoMsg = array();
            $filds = array();
            if (FileUpload::fileExist(array("tmp_name" => $files["tmp_name"][$i]))) {
                $nome = FileUpload::generateRandomString(10) . $files["name"][$i];
                $filds[":nome_img"] = $nome;
                $filds[":type_img"] = $files["type"][$i];
                if ($type == 0) {
                    $filds[":file64"] = FileUpload::toBase64(array("tmp_name" => $files["tmp_name"][$i]));
                    $filds[":patch"] = "";
                }else{
                    $filds[":patch"] = FileUpload::getPathStore();
                    $filds[":file64"] = "";
                    $retornoMsg[] = FileUpload::storeFile($files["tmp_name"][$i], $nome);
                }
            }else{
                $retornoMsg[] = array("status" => "danger", "msg" => "Arquivo invalido!");
            }
            $retorno[] = array("erro" => $retornoMsg , "filds" => $filds);
        } 
        return $retorno;
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function fileExist($file) {
        if (isset($file) && file_exists($file['tmp_name'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function toBase64($file) {
        $data = file_get_contents($file['tmp_name']);
        $base64 = base64_encode($data);
        return $base64;
    }

    public static function getPathStore() {
        return 'res' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . 'vault' . DIRECTORY_SEPARATOR;
    }

    public static function storeFile($file, $nome) {
        $erros = array();
        $pathUpload = FileUpload::getPathStore();
        if (!file_exists($pathUpload)) {
            $folder = mkdir($pathUpload, 0777, true);
        }
        if (!move_uploaded_file($file, $pathUpload . $nome)) {
            $erros = array("status" => "danger", "msg" => "Não foi possível fazer upload do arquivo: " . $nome . ", tente novamente mais tarde!");
        }
        return $erros;
    }

//    public static function validaTipoPDF($files) {
//        $erros = array();
//        $permite = array('image/jpg', 'image/jpeg', 'image/png');
//        if (!in_array($files['type'], $permite)) {
//            $erros[] =  array("status" => "danger", "msg" => "Arquivo não suportado! Selecione um arquivo PDF para continuar!");
//        }
//        return $erros;
//    }

    public static function validaTipoImagem($files) {
        $erros = array();
        $permite = array('image/jpg', 'image/jpeg', 'image/png');
        if (!in_array($files['type'], $permite)) {
            $erros[] = array("status" => "danger", "msg" => "Arquivo não suportado! Selecione um arquivo JPG, JPEG ou PNG para continuar!");
        }
        return $erros;
    }

    public static function validaTipoAudio($files) {
        $erros = array();
        $permite = array('audio/mpeg', 'audio/mp3', 'audio/mpeg3', 'audio/x-mpeg', 'audio/x-mpeg-3');
        if (!in_array($files['type'], $permite)) {
            $erros[] = array("status" => "danger", "msg" => "Arquivo não suportado! Selecione um arquivo MPEG ou MP3 para continuar!");
        }
        return $erros;
    }

}

?>