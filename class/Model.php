<?php

class Model {

    private $values = [];
    public $TB_NAME = "";
    public $campos = [];
    public $totalQuery = 0;

    public function getLimitQuery() {
        return 10;
    }

    public function __call($name, $args) {
        $method = substr($name, 0, 3);

        $fieldName = substr($name, 3, strlen($name));

        switch ($method) {
            case "get":
                try {
                    return $this->values[$fieldName];
                } catch (Exception $exc) {
                    //echo $exc->getTraceAsString();
                    return NULL;
                }
                break;
            case "set":
                $this->values[$fieldName] = $args[0];
                break;
        }
    }

    public function setValues($data) {
        foreach ($data as $key => $value) {
            $this->{"set" . $key}($value);
        }
    }

    public function getValues() {
        return $this->values;
    }

    public function selectDB($filds = array(), $fildsWhere = array(), $fildsOrder = array(), $offset = -1) {
        date_default_timezone_set('America/Sao_Paulo');
        $query = "SELECT ";
        $count = 0;
        $retorno = array();
        if (count($filds) == 0) {
            $query .= " * ";
        } else {
            foreach ($filds as $key) {
                if ($count == 0) {
                    $query .= $key;
                } else {
                    $query .= ", " . $key;
                }
                $count ++;
            }
        }
        $query .= " FROM " . $this->TB_NAME;
        $query .= " WHERE ";
        $query .= " deleted = :deleted ";
        if (count($fildsWhere) > 0) {
            foreach ($fildsWhere as $key => $value) {
                $query .= "AND " . substr($key, 1) . " = " . $key . " ";
            }
        }
        $fildsWhere[":deleted"] = 0;
        if (count($fildsOrder) > 0) {
            $count = 0;
            $query .= " ORDER BY ";
            foreach ($fildsOrder as $key => $value) {
                if ($count == 0) {
                    $query .= substr($key, 1) . " " . $value;
                } else {
                    $query .= ", " . substr($key, 1) . " " . $value;
                }
                $count ++;
            }
        }
        if ($offset >= 0) {
            $query .= " LIMIT " . $this->getLimitQuery() . " OFFSET " . ($offset * $this->getLimitQuery());
        }

        $sql = new Sql();
//        var_dump($query);
//        echo '<br/>';
//        var_dump($fildsWhere);
        $stmt = $sql->select($query, $fildsWhere);
        $this->totalQuery = $this->countSelectDB($fildsWhere);
        if (false === $stmt) {
            return null;
        } else {
            return $stmt;
        }
    }

    public function countSelectDB($fildsWhere = array()) {
        date_default_timezone_set('America/Sao_Paulo');
        $query = "SELECT ";
        $count = 0;
        $retorno = array();
        $query .= " id ";
        $query .= " FROM " . $this->TB_NAME;
        if (count($fildsWhere) > 0) {
            $count = 0;
            $query .= " WHERE ";
            foreach ($fildsWhere as $key => $value) {
                if ($count == 0) {
                    $query .= substr($key, 1) . " = " . $key;
                } else {
                    $query .= ", " . substr($key, 1) . " = " . $key;
                }
                $count ++;
            }
        }
        $sql = new Sql();
        $stmt = $sql->select($query, $fildsWhere);

        if (false === $stmt) {
            return 0;
        } else {
            return count($stmt);
        }
    }

    public function loadById($id) {
        $fildsWhere = array(
            ":id" => $id,
            ":deleted" => 0
        );
        $results = $this->selectDB(array(), $fildsWhere);
        if (count($results) > 0) {
            $this->setValues($results[0]);
        }
    }

    public function getPaginacao($rota, $pag) {
        $sql = new Sql();
        $total = $this->totalQuery;
        $pages = ceil($total / $this->getLimitQuery());
        $ret = '';
        $ret .= '<nav >';
        $ret .= '        <ul class="pagination">';
        if ($pages > 1 && $pag > 0) {
            $ret .= '            <li class="page-item"><a class="page-link" href="' . $rota . '/pag/' . ($pag - 1) . '">Anterior</a></li>';
        }
        for ($i = 0; $i < $pages; $i++) {
            $ret .= '            <li class="page-item"><a class="page-link" href="' . $rota . '/pag/' . $i . '">' . ($i + 1) . '</a></li>';
        }
        if ($pages > 1 && $pag < ($pages - 1)) {
            $ret .= '            <li class="page-item"><a class="page-link" href="' . $rota . '/pag/' . ($pag + 1) . '">Próxima</a></li>';
        }
        $ret .= '        </ul>';
        $ret .= '</nav>';
        return $ret;
    }

    public function insertDB($filds = array(), $fildsValidacao = array()) {
        date_default_timezone_set('America/Sao_Paulo');
        $retorno = array();
        $retorno = $this->validaInformacoes($fildsValidacao);
        if (count($retorno) == 0) {
            foreach ($filds as $key => $value) {
                if (null == $value) {
                    $filds[$key] = $this->{"get" . substr($key, 1)}();
                }
            }
            $query = "INSERT INTO " . $this->TB_NAME . " ";
            $query1 = "data_criacao, user_criacao, deleted ";
            $query2 = ":data_criacao, :user_criacao, :deleted ";

            foreach ($filds as $key => $value) {
                $query1 .= ", " . substr($key, 1);
                $query2 .= ", " . $key;
            }
            $query .= "( " . $query1 . " ) VALUES ( " . $query2 . " )";

            $filds[":data_criacao"] = date("Y-m-d H:i:s");
            $filds[":user_criacao"] = Usuario::getLogadoLog();
            $filds[":deleted"] = 0;

            $sql = new Sql();
//            var_dump($filds);
//            echo '<br/>';
//            echo $query;
//            echo '<br/>';
//            exit();
            $stmt = $sql->query($query, $filds);
//        echo $stmt->errorCode();
//        var_dump($stmt->errorInfo());
//        exit();
            if (false === $stmt) {
                array_push($retorno, array("status" => "danger", "msg" => "Falha ao gravar a sua solicitação, tente novamente mais tarde!"));
            } else {
                array_push($retorno, array("status" => "success", "msg" => "Gravado com sucesso!"));
            }
        }
        return $retorno;
    }

    public function updateDB($filds = array(), $fildsWhere = array(), $fildsValidacao = array()) {
        date_default_timezone_set('America/Sao_Paulo');
        $retorno = array();
        $retorno = $this->validaInformacoes($fildsValidacao);
        if (count($retorno) == 0) {
            foreach ($filds as $key => $value) {
                if (null == $value) {
                    $filds[$key] = $this->{"get" . substr($key, 1)}();
                }
            }
            $query = "UPDATE " . $this->TB_NAME . " SET ";
            $query .= "data_alteracao = :data_alteracao, user_alt = :user_alt ";
            $arrayParam = array();
            foreach ($filds as $key => $value) {
                $query .= ", " . substr($key, 1) . " = " . $key;
            }
            $query .= " WHERE ";
            $count = 0;
            foreach ($fildsWhere as $key => $value) {
                if ($count == 0) {
                    $query .= substr($key, 1) . " = " . $key;
                } else {
                    $query .= ", " . substr($key, 1) . " = " . $key;
                }
                $count ++;
            }
            $filds[":data_alteracao"] = date("Y-m-d H:i:s");
            $filds[":user_alt"] = Usuario::getLogadoLog();
            $sql = new Sql();
            $arrayParam = $filds + $fildsWhere;
            $stmt = $sql->query($query, $arrayParam);
//            var_dump($stmt);
//            echo '<br/>';
//            echo $stmt->errorCode();
//            echo '<br/>';
//            var_dump($stmt->errorInfo());
//            exit();
            if (false === $stmt) {
                array_push($retorno, array("status" => "danger", "msg" => "Falha ao atualizar as informações, tente novamente mais tarde!"));
            } else {
                array_push($retorno, array("status" => "success", "msg" => "Atualizado com sucesso!"));
            }
        }
        return $retorno;
    }

    public function deleteDB($fildsWhere = array()) {
        date_default_timezone_set('America/Sao_Paulo');
        $query = "DELETE FROM " . $this->TB_NAME . " WHERE ";
        $count = 0;
        $retorno = array();
        foreach ($fildsWhere as $key => $value) {
            if ($count == 0) {
                $query .= substr($key, 1) . " = " . $key;
            } else {
                $query .= ", " . substr($key, 1) . " = " . $key;
            }
            $count ++;
        }
        $sql = new Sql();
        $stmt = $sql->query($query, $fildsWhere);
        if (false === $stmt) {
            array_push($retorno, array("status" => "danger", "msg" => "Falha ao deletar as informações, tente novamente mais tarde!"));
        } else {
            array_push($retorno, array("status" => "success", "msg" => "Deletado com sucesso!"));
        }
        return $retorno;
    }

    public function markDeleteDBById($id) {
        $filds = array(
            ":user_del" => Usuario::getLogadoLog(),
            ":deleted" => 1,
            ":data_alteracao" => date("Y-m-d H:i:s")
        );
        $fildsWhere = array(
            ":id" => $id
        );
        return $this->updateDB($filds, $fildsWhere);
    }

    public function validaInformacoes($fildsForm = array()) {
        $data = $_POST;
        $retorno = array();
        if (count($fildsForm) > 0) {
            foreach ($fildsForm as $key) {
                if (array_key_exists($key, $data)) {
                    if (!is_numeric($data[$key]) && empty($data[$key])) {
                        $retorno[] = array("status" => "danger", "msg" => "O campo: " . $key . " é obrigatório!");
                    }
                }
            }
            if (count($retorno) == 0) {
                $this->setValues($data);
            }
        } else {
            $this->setValues($data);
        }
        return $retorno;
    }

}
