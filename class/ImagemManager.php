<?php

class ImagemManager {

    public static function escreveeGuarda($filename, $texto, $font = 5, $x = 0, $y = 0) {
        $imagem = imagecreatefrompng($filename);
        $titleColor = imagecolorallocate($imagem, 0, 0, 0);
        $gray = imagecolorallocate($imagem, 100, 100, 100);
        imagestring($imagem, $font, $x, $y, $texto, $titleColor);
        imagepng($imagem, "banco_imagem/upload-" . date("Y-m-d H:i:s") . "png");
        imagedestroy($imagem);
    }

    public static function resize($args) {
        $token = $args[0];
        $width = $args[1];
        $imgValt = new ImagemValt();
        $imgValt->loadByToken($token);
        if (null != $imgValt->getid()) {
            $image = imagecreatefromstring(base64_decode($imgValt->getdata()));
            $orig_width = imagesx($image);
            $orig_height = imagesy($image);
            $height = (($orig_height * $width) / $orig_width);
            $new_image = imagecreatetruecolor($width, $height);
            imagecopyresized($new_image, $image,
                    0, 0, 0, 0,
                    $width, $height,
                    $orig_width, $orig_height);
            imagejpeg($new_image);
        }
    }
    

}
