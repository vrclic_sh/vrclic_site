<?php

require_once("vendor/autoload.php");

if (!isset($_SESSION)) {
    session_start();
}

spl_autoload_register(function($class_name) {
    $filename = "class" . DIRECTORY_SEPARATOR . $class_name . ".php";
    if (file_exists(($filename))) {
        require_once($filename);
    }
});

spl_autoload_register(function($class_name) {
    $filename = "class" . DIRECTORY_SEPARATOR ."model" . DIRECTORY_SEPARATOR. $class_name . ".php";
    if (file_exists(($filename))) {
        require_once($filename);
    }
});

//ControllerRouter::startRouter();
 $controller = new ControllerRouter();
 $controller->startRouter();
?>
