<?php
$ObjetoClass = new Equipe();
$pathPage = "equipe";
$namePage = "Equipe";
$pag = 0;

if ($typeReq == "POST" && isset($_POST) && isset($args[1]) && $args[1] == "insert") {
    $msgErro = $ObjetoClass->insertForm();
}
if ($typeReq == "POST" && isset($_POST) && isset($args[1]) && isset($args[2]) && $args[1] == "editar") {
    $msgErro = $ObjetoClass->updateForm($args[2]);
}

if ($typeReq == "POST" && isset($_POST) && isset($args[1]) && isset($args[2]) && $args[1] == "deletar") {
    $msgErro = $ObjetoClass->deleteForm($args[2]);
}

if ($typeReq == "GET" && isset($_POST) && isset($args[1]) && isset($args[2]) && $args[1] == "pag") {
    $pag = $args[2];
}
?>


<?php require 'views/adm/header.php'; ?> 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo mb_strtoupper($namePage, 'UTF-8'); ?>
            <small>Administar <?php echo $namePage; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $namePage; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if (isset($msgErro)) {
                ErrorHandler::printErrorForm($msgErro);
            }
            ?>

            <div class="col-lg-12 col-xs-12">               
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">NOVO <?php echo mb_strtoupper($namePage, 'UTF-8'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-dark">
                                <div class="panel-heading" role="tab" id="headingOne">                                                  
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <span class="panel-title"><i class="fa fa-user-plus"></i> <b>Cadastrar Novo <?php echo $namePage; ?></b></span>
                                    </a>                                                    
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <form class="material" role="form" action="/admin/a/<?php echo $pathPage; ?>/insert" method="POST" enctype="multipart/form-data">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sexo">Sexo</label>
                                                    <select class="form-control" id="sexo" name="sexo">
                                                        <option value="M">Masculino</option>
                                                        <option value="F">Feminino</option>
                                                        <option value="O">Outro</option>
                                                        <option value="N">Não informado</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="status">Status</label>
                                                    <select class="form-control" id="status" name="status">
                                                        <option value="0" >INATIVO</option>
                                                        <option value="1" >ATIVO</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="telefone">Telefône</label>
                                                    <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefône"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nome">Celular</label>
                                                    <input type="text" class="form-control" id="celular" name="celular" placeholder="Celular"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">E-Mail</label>
                                                    <input type="text" class="form-control" id="email" name="email" placeholder="E-Mail"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="facebook">Facebook</label>
                                                    <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="instagram">Instagram</label>
                                                    <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Instagram"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="linkedin">Linkedin</label>
                                                    <input type="text" class="form-control" id="linkedin" name="linkedin" placeholder="Linkedin"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="site">Site</label>
                                                    <input type="text" class="form-control" id="site" name="site" placeholder="Site"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="twitter">Twitter</label>
                                                    <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="youtube">Youtube</label>
                                                    <input type="text" class="form-control" id="youtube" name="youtube" placeholder="Youtube"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="twitch">Twitch</label>
                                                    <input type="text" class="form-control" id="twitch" name="twitch" placeholder="Twitch"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tinder">Tinder</label>
                                                    <input type="text" class="form-control" id="tinder" name="tinder" placeholder="Tinder"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="whatsapp">WhatsApp</label>
                                                    <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="WhatsApp"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="funcao">Função</label>
                                                    <input type="text" class="form-control" id="funcao" name="funcao" placeholder="Função"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="descricao">Conteúdo</label>
                                                    <textarea name="descricao" placeholder="Conteúdo" class="form-control summernote"></textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label for="imagem01">Foto</label>
                                                    <input type="file" name="imagem01frm" id="imagem01" class="form-control"/>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-address-card"></i> SALVAR</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>                                            
                        </div>                       
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-xs-12">               
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo mb_strtoupper($namePage, 'UTF-8'); ?> CADASTRADOS</h3>
                    </div>
                    <div class="box-body">                        
                        <table class="table table-bordered table-striped table-hover table-responsive">
                            <tr>
                                <th>TÍTULO</th>
                                <th>STATUS</th>
                                <th>CRIADO EM</th>
                                <th>AÇÃO</th>
                            </tr>
                            <?php
                            foreach ($ObjetoClass->getListPag($pag) as $value) {
                                ?> 
                                <tr>
                                    <td><?php echo $value["nome"]; ?> </td>
                                    <td><?php echo ($value["status"] == 1 ? "ATIVO" : "INATIVO"); ?> </td>
                                    <td><?php echo DateConverter::toForm($value["data_criacao"]); ?> </td>
                                    <td>
                                        <button class="btn btn-warning btn-sm" data-toggle="modal" data-target=".bs-edita-modal-<?php echo $value["id"]; ?>"><i class="fa fa-edit"></i> Editar</button>                                        
                                        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target=".bs-deletar-modal-<?php echo $value["id"]; ?>"><i class="fa fa-trash-o"></i> Excluir</button>                                        
                                    </td>
                                </tr>
                                <?php
                            }
                            ?> 
                        </table>
                        <?php
                        echo $ObjetoClass->getPaginacao("/admin/a/" . $pathPage, $pag);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row"> 
            <div class="col-lg-12 col-xs-12">                

                <!-- /.box-body -->
            </div>
        </div>

        <?php
        foreach ($ObjetoClass->getListPag($pag) as $value) {
            ?> 
            <div class="modal fade bs-edita-modal-<?php echo $value["id"]; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <button type="button" class="close" data-dismiss="modal" title="Fechar esta Janela"><i class="fa fa-close"></i></button>
                                <h3 class="box-title">EDITAR <?php echo mb_strtoupper($namePage, 'UTF-8'); ?></h3>
                            </div>
                            <div class="box-body">
                                <form class="material" role="form" action="/admin/a/<?php echo $pathPage; ?>/editar/<?php echo $value["id"]; ?>" method="POST" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="nome">Nome</label>
                                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="<?php echo $value["nome"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="sexo">Sexo</label>
                                            <select class="form-control" id="sexo" name="sexo">
                                                <option value="M" <?php echo ($value["sexo"] == "M" ? "selected" : ""); ?>>Masculino</option>
                                                <option value="F" <?php echo ($value["sexo"] == "F" ? "selected" : ""); ?>>Feminino</option>
                                                <option value="O" <?php echo ($value["sexo"] == "O" ? "selected" : ""); ?>>Outro</option>
                                                <option value="N" <?php echo ($value["sexo"] == "N" ? "selected" : ""); ?>>Não informado</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select class="form-control" id="status" name="status">
                                                <option value="0" <?php echo ($value["status"] == 0 ? "selected" : "") ?>>INATIVO</option>
                                                <option value="1" <?php echo ($value["status"] == 1 ? "selected" : "") ?>>ATIVO</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone">Telefône</label>
                                            <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefône" value="<?php echo $value["telefone"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="nome">Celular</label>
                                            <input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" value="<?php echo $value["celular"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">E-Mail</label>
                                            <input type="text" class="form-control" id="email" name="email" placeholder="E-Mail" value="<?php echo $value["email"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="facebook">Facebook</label>
                                            <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook" value="<?php echo $value["facebook"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="instagram">Instagram</label>
                                            <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Instagram" value="<?php echo $value["instagram"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="linkedin">Linkedin</label>
                                            <input type="text" class="form-control" id="linkedin" name="linkedin" placeholder="Linkedin" value="<?php echo $value["linkedin"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="site">Site</label>
                                            <input type="text" class="form-control" id="site" name="site" placeholder="Site" value="<?php echo $value["site"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="twitter">Twitter</label>
                                            <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter" value="<?php echo $value["twitter"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="youtube">Youtube</label>
                                            <input type="text" class="form-control" id="youtube" name="youtube" placeholder="Youtube" value="<?php echo $value["youtube"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="twitch">Twitch</label>
                                            <input type="text" class="form-control" id="twitch" name="twitch" placeholder="Twitch" value="<?php echo $value["twitch"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="tinder">Tinder</label>
                                            <input type="text" class="form-control" id="tinder" name="tinder" placeholder="Tinder" value="<?php echo $value["tinder"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="whatsapp">WhatsApp</label>
                                            <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="WhatsApp" value="<?php echo $value["whatsapp"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="funcao">Função</label>
                                            <input type="text" class="form-control" id="funcao" name="funcao" placeholder="Função" value="<?php echo $value["funcao"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="descricao">Conteúdo</label>
                                            <textarea name="descricao" placeholder="Conteúdo" class="form-control summernote"><?php echo $value["descricao"]; ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="imagem01Atu">Atual</label>
                                            <img src="/img/<?php echo $value["token_img"] ?>/600/1"  class="img-responsive" alt=""/>
                                        </div>
                                        <div class="form-group">
                                            <label for="imagem01">Foto</label>
                                            <input type="file" name="imagem01frm" id="imagem01" class="form-control"/>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> SALVAR ALTERAÇÕES</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> CANCELAR</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?> 


        <?php
        $obj = $ObjetoClass;
        foreach ($obj->getListPag($pag) as $value) {
            ?> 
            <div class="modal fade bs-deletar-modal-<?php echo $value["id"]; ?>" tabindex="-1" role="dialog" aria-labelledby="Deletar">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <button type="button" class="close" data-dismiss="modal" title="Fechar esta Janela"><i class="fa fa-close"></i></button>
                                <h3 class="box-title">DELETAR <?php echo mb_strtoupper($namePage, 'UTF-8'); ?></h3>
                            </div>
                            <div class="box-body">
                                <form class="material" role="form" action="/admin/a/<?php echo $pathPage; ?>/deletar/<?php echo $value["id"]; ?>" method="POST" >
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="nome">Nome: </label>
                                            <?php echo $value["nome"]; ?>
                                        </div>                                                                       
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> EXCLUIR</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> CANCELAR</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?> 


    </section>
</div>
<!-- /.content-wrapper -->

<?php require 'views/adm/footer.php'; ?>