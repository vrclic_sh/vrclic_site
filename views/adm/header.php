<?php
$userLogado = new Usuario();
//$userLogado = $userLogado->getLogado();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <title><?php echo $empresa->getnome(); ?> - ADMIN</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/res/adm/bootstrap/css/bootstrap.css"/>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="/res/adm/dist/css/AdminLTE.css"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/res/adm/dist/css/skins/_all-skins.css"/>
        <!-- iCheck -->
        <link rel="stylesheet" href="/res/adm/plugins/iCheck/flat/red.css"/>
        <!-- Morris chart -->
<!--        <link rel="stylesheet" href="/res/adm/plugins/morris/morris.css"/>-->
        <!-- jvectormap -->
<!--        <link rel="stylesheet" href="/res/adm/plugins/jvectormap/jquery-jvectormap-1.2.2.css"/>-->
        <!-- Date Picker -->
<!--        <link rel="stylesheet" href="/res/adm/plugins/datepicker/datepicker3.css"/>-->
        <!-- Daterange picker -->
<!--        <link rel="stylesheet" href="/res/adm/plugins/daterangepicker/daterangepicker.css"/>-->
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="/res/adm/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css"/>        

        <link rel="stylesheet" href="/res/adm/css/summernote.css"/>      

        <link rel="stylesheet" href="/res/adm/css/styletable.css"/>

        <link rel="stylesheet" href="/res/adm/css/vrclic.css"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->



    </head>
    <body class="hold-transition skin-black sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/admin" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><?php echo $empresa->getnome(); ?></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><?php echo $empresa->getnome(); ?></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-user-circle-o"></i>
                                    <span class="hidden-xs"><?php echo $userLogado->getnome(); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header" style="color: #FFFFFF"><br/> 
                                        <i class="fa fa-user-circle-o fa-3x"></i><br/>                                        
                                        <?php echo $userLogado->getnome(); ?><br/>
                                        <small>Cadastrado em: <?php echo $userLogado->getdata_criacao(); ?></small>                                        
                                    </li>

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="/sair" class="btn btn-danger btn-flat"><i class="fa fa-sign-out"></i> Sair</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">

                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MENU</li>
                        <li><a href="/admin/a/grupoportfolio"><i class="fa fa-newspaper-o"></i> <span>Grupo Portfolio</span></a></li>
                        <li><a href="/admin/a/portfolio"><i class="fa fa-newspaper-o"></i> <span>Portfólio</span></a></li>
                        <li><a href="/admin/a/equipe"><i class="fa fa-newspaper-o"></i> <span>Equipe</span></a></li>ia</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>


