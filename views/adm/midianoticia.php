<?php
$ObjetoClass = new MidiaNoticia();
$pathPage = "midianoticia";
$namePage = "Midia Noticia";
$pag = 0;

if (isset($args[2])) {
    $codSuperior = intval($args[2]);
}

if ($typeReq == "POST" && isset($_POST) && isset($args[1]) && isset($args[2]) && $args[1] == "insert") {
    $msgErro = $ObjetoClass->insertForm($codSuperior, $_FILES['arquivos']);
}
//if ($typeReq == "POST" && isset($_POST) && isset($args[1]) && isset($args[2]) && isset($args[3]) && $args[1] == "editar") {
//    $msgErro = $ObjetoClass->updateForm($args[3]);
//}

if ($typeReq == "GET" && isset($_POST) && isset($args[1]) && isset($args[2]) && isset($args[3]) && $args[1] == "deletar") {
    $msgErro = $ObjetoClass->deleteForm($args[3]);
}

if ($typeReq == "GET" && isset($_POST) && isset($args[1]) && isset($args[2]) && isset($args[3]) && $args[1] == "pag") {
    $pag = $args[3];
}
?>


<?php require 'views/adm/header.php'; ?> 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo mb_strtoupper($namePage, 'UTF-8'); ?>
            <small>Administar <?php echo $namePage; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $namePage; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if (isset($msgErro)) {
                ErrorHandler::printErrorForm($msgErro);
            }
            ?>

            <div class="col-lg-12 col-xs-12">               
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">NOVO <?php echo mb_strtoupper($namePage, 'UTF-8'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-dark">
                                <div class="panel-heading" role="tab" id="headingOne">                                                  
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <span class="panel-title"><i class="fa fa-user-plus"></i> <b>Cadastrar Novo <?php echo $namePage; ?></b></span>
                                    </a>                                                    
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <form class="material" role="form" action="/admin/a/<?php echo $pathPage; ?>/insert/<?php echo $codSuperior; ?>" method="POST" enctype="multipart/form-data">
                                            <div class="box-body">
                                                <input type="file" multiple="multiple" name="arquivos[]" class="form-control" placeholder="Selecionar as Fotos"/>
                                            </div>
                                            <!-- /.box-body -->

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-address-card"></i> ENVIAR</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>                                            
                        </div>                       
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-xs-12">               
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo mb_strtoupper($namePage, 'UTF-8'); ?> CADASTRADOS</h3>
                    </div>
                    <div class="box-body">                        
                        <table class="table table-bordered table-striped table-hover table-responsive">
                            <tr>
                                <th>FOTO</th>
                                <th>CRIADO EM</th>
                                <th>AÇÃO</th>
                            </tr>
                            <?php
                            foreach ($ObjetoClass->getListNoticia($codSuperior) as $value) {
                                ?> 
                                <tr>
                                    <td><img src="/imgnews/<?php echo $value["token"]; ?>/600/1"  class="img-responsive" alt=""/></td>
                                    <td><?php echo DateConverter::toForm($value["data_criacao"]); ?> </td>
                                    <td>
                                        <a href="/admin/a/<?php echo $pathPage; ?>/deletar/<?php echo $codSuperior; ?>/<?php echo $value["id"]; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Excluir</a>                                        
                                    </td>
                                </tr>
                                <?php
                            }
                            ?> 
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row"> 
            <div class="col-lg-12 col-xs-12">                

                <!-- /.box-body -->
            </div>
        </div>

        


    </section>
</div>
<!-- /.content-wrapper -->

<?php require 'views/adm/footer.php'; ?>