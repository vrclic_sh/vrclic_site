
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b></b>
    </div>
    <strong>Copyright &copy; 2017 - <?php echo date('Y'); ?> <a href="http://vrclic.com.br" target="_blank">VR CLIC</a>.</strong> Direitos Reservados
</footer>

<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/res/adm/js/jquery_3_4_1.js"></script>
<!--<script src="/res/adm/js/jquery.js"></script>-->
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="/res/adm/bootstrap/js/bootstrap.js"></script>
 Morris.js charts 
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/res/adm/plugins/morris/morris.js"></script>-->
 Sparkline 
<!--<script src="/res/adm/plugins/sparkline/jquery.sparkline.js"></script>-->
<!-- jvectormap -->
<!--<script src="/res/adm/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/res/adm/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->
<!-- jQuery Knob Chart -->
<!--<script src="/res/adm/plugins/knob/jquery.knob.js"></script>-->
<!-- daterangepicker -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="/res/adm/plugins/daterangepicker/daterangepicker.js"></script>-->
<!-- datepicker -->
<!--<script src="/res/adm/plugins/datepicker/bootstrap-datepicker.js"></script>-->
<!-- Bootstrap WYSIHTML5 -->
<!--<script src="/res/adm/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>-->
<!-- Slimscroll -->
<!--<script src="/res/adm/plugins/slimScroll/jquery.slimscroll.js"></script>-->
<!-- FastClick -->
<!--<script src="/res/adm/plugins/fastclick/fastclick.js"></script>-->
<!-- AdminLTE App -->
<!--<script src="/res/adm/dist/js/app.js"></script>-->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="/res/adm/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<!--<script src="/res/adm/dist/js/demo.js"></script>-->

<script src="/res/adm/js/jquery.dataTables.min.js"></script>

<script src="/res/adm/js/summernote.js"></script>
<script src="/res/adm/js/summernote-pt-BR.js"></script>

<script src="/res/adm/js/jquery.mask.js"></script>


<script src="/res/adm/js/lazyload.js"></script>

<script>
    $(function () {
        $('.datepicker').datepicker();
    });
    
    $('.dinheiro').mask('#.##0,00', {reverse: true});
    
    $('#tblista').dataTable();

    $('.summernote').summernote({
        height: 300,
        lang: 'pt-BR'
    });
</script>

</body>
</html>
