<?php
$ObjetoClass = new Portfolio();
$pathPage = "portfolio";
$namePage = "Portfólio";
$pag = 0;

if ($typeReq == "POST" && isset($_POST) && isset($args[1]) && $args[1] == "insert") {
    $msgErro = $ObjetoClass->insertForm();
}
if ($typeReq == "POST" && isset($_POST) && isset($args[1]) && isset($args[2]) && $args[1] == "editar") {
    $msgErro = $ObjetoClass->updateForm($args[2]);
}

if ($typeReq == "POST" && isset($_POST) && isset($args[1]) && isset($args[2]) && $args[1] == "deletar") {
    $msgErro = $ObjetoClass->deleteForm($args[2]);
}

if ($typeReq == "GET" && isset($_POST) && isset($args[1]) && isset($args[2]) && $args[1] == "pag") {
    $pag = $args[2];
}
?>


<?php require 'views/adm/header.php'; ?> 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo mb_strtoupper($namePage, 'UTF-8'); ?>
            <small>Administar <?php echo $namePage; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $namePage; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if (isset($msgErro)) {
                ErrorHandler::printErrorForm($msgErro);
            }
            ?>

            <div class="col-lg-12 col-xs-12">               
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">NOVO <?php echo mb_strtoupper($namePage, 'UTF-8'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-dark">
                                <div class="panel-heading" role="tab" id="headingOne">                                                  
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <span class="panel-title"><i class="fa fa-user-plus"></i> <b>Cadastrar Novo <?php echo $namePage; ?></b></span>
                                    </a>                                                    
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <form class="material" role="form" action="/admin/a/<?php echo $pathPage; ?>/insert" method="POST" enctype="multipart/form-data">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tb_grupo_portfolio_id">Categoria</label>
                                                    <select class="form-control" id="tb_grupo_portfolio_id" name="tb_grupo_portfolio_id">
                                                        <?php
                                                        $gp = new GrupoPortfolio();
                                                        foreach ($gp->getAllAtivas() as $valueGP) {
                                                            echo '<option value = "' . $valueGP["id"] . '" >' . $valueGP["titulo"] . '</option >';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="status">Status</label>
                                                    <select class="form-control" id="status" name="status">
                                                        <option value="0" >INATIVO</option>
                                                        <option value="1" >ATIVO</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="link">Link</label>
                                                    <input type="text" class="form-control" id="link" name="link" placeholder="Link"/>
                                                </div>

                                                <div class="form-group">
                                                    <label for="data_publicacao">Data Publicação</label>
                                                    <input type="datetime" class="form-control" id="data_ini" name="data_publicacao" placeholder="Data Publicação" data-mask="00/00/0000 00:00:00" data-mask-reverse="true"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="imagem01">Foto</label>
                                                    <input type="file" name="imagem01frm" id="imagem01" class="form-control"/>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-address-card"></i> SALVAR</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>                                            
                        </div>                       
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-xs-12">               
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo mb_strtoupper($namePage, 'UTF-8'); ?> CADASTRADOS</h3>
                    </div>
                    <div class="box-body">                        
                        <table class="table table-bordered table-striped table-hover table-responsive">
                            <tr>
                                <th>TÍTULO</th>
                                <th>STATUS</th>
                                <th>CRIADO EM</th>
                                <th>AÇÃO</th>
                            </tr>
                            <?php
                            foreach ($ObjetoClass->getListPag($pag) as $value) {
                                ?> 
                                <tr>
                                    <td><?php echo $value["nome"]; ?> </td>
                                    <td><?php echo ($value["status"] == 1 ? "ATIVO" : "INATIVO"); ?> </td>
                                    <td><?php echo DateConverter::toForm($value["data_criacao"]); ?> </td>
                                    <td>
                                        <a href="/admin/a/midianoticia/load/<?php echo $value["id"]; ?>" class="btn btn-primary btn-sm"><i class="fa fa-picture-o"></i> Midias</a>
                                        <button class="btn btn-warning btn-sm" data-toggle="modal" data-target=".bs-edita-modal-<?php echo $value["id"]; ?>"><i class="fa fa-edit"></i> Editar</button>                                        
                                        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target=".bs-deletar-modal-<?php echo $value["id"]; ?>"><i class="fa fa-trash-o"></i> Excluir</button>                                        
                                    </td>
                                </tr>
                                <?php
                            }
                            ?> 
                        </table>
                        <?php
                        echo $ObjetoClass->getPaginacao("/admin/a/" . $pathPage, $pag);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row"> 
            <div class="col-lg-12 col-xs-12">                

                <!-- /.box-body -->
            </div>
        </div>

        <?php
        foreach ($ObjetoClass->getListPag($pag) as $value) {
            ?> 
            <div class="modal fade bs-edita-modal-<?php echo $value["id"]; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <button type="button" class="close" data-dismiss="modal" title="Fechar esta Janela"><i class="fa fa-close"></i></button>
                                <h3 class="box-title">EDITAR <?php echo mb_strtoupper($namePage, 'UTF-8'); ?></h3>
                            </div>
                            <div class="box-body">
                                <form class="material" role="form" action="/admin/a/<?php echo $pathPage; ?>/editar/<?php echo $value["id"]; ?>" method="POST" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="nome">Nome</label>
                                            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome"  value="<?php echo $value["nome"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="tb_grupo_portfolio_id">Categoria</label>
                                            <select class="form-control" id="tb_grupo_portfolio_id" name="tb_grupo_portfolio_id">
                                                <?php
                                                $gp = new GrupoPortfolio();
                                                foreach ($gp->getAllAtivas() as $valueGP) {
                                                    echo '<option value = "' . $valueGP["id"] . '" ' . ($valueGP["id"] == $value["tb_grupo_portfolio_id"] ? "selected" : "") . '>' . $valueGP["titulo"] . '</option >';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select class="form-control" id="status" name="status">
                                                <option value="0" <?php echo ($value["status"] == 0 ? "selected" : "") ?>>INATIVO</option>
                                                <option value="1" <?php echo ($value["status"] == 1 ? "selected" : "") ?>>ATIVO</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="link">Link</label>
                                            <input type="text" class="form-control" id="link" name="link" placeholder="Link"  value="<?php echo $value["link"]; ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="data_publicacao">Data Publicação</label>
                                            <input type="datetime" class="form-control" id="data_ini" name="data_publicacao" placeholder="Data Publicação" data-mask="00/00/0000 00:00:00" data-mask-reverse="true"  value="<?php echo DateConverter::toFormDT($value["data_publicacao"]); ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="imagem01Atu">Atual</label>
                                            <img src="/img/<?php echo $value["token_img"] ?>/600/1"  class="img-responsive" alt=""/>
                                        </div>
                                        <div class="form-group">
                                            <label for="imagem01">Foto</label>
                                            <input type="file" name="imagem01frm" id="imagem01" class="form-control"/>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> SALVAR ALTERAÇÕES</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> CANCELAR</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?> 


        <?php
        $obj = $ObjetoClass;
        foreach ($obj->getListPag($pag) as $value) {
            ?> 
            <div class="modal fade bs-deletar-modal-<?php echo $value["id"]; ?>" tabindex="-1" role="dialog" aria-labelledby="Deletar">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <button type="button" class="close" data-dismiss="modal" title="Fechar esta Janela"><i class="fa fa-close"></i></button>
                                <h3 class="box-title">DELETAR <?php echo mb_strtoupper($namePage, 'UTF-8'); ?></h3>
                            </div>
                            <div class="box-body">
                                <form class="material" role="form" action="/admin/a/<?php echo $pathPage; ?>/deletar/<?php echo $value["id"]; ?>" method="POST" >
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="nome">Título: </label>
                                            <?php echo $value["nome"]; ?>
                                        </div>                                                                       
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> EXCLUIR</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> CANCELAR</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?> 


    </section>
</div>
<!-- /.content-wrapper -->

<?php require 'views/adm/footer.php'; ?>