<?php
if ($typeReq == "POST") {
    $msgLogin = array();
    $usuario = new Usuario();
    if (isset($_POST["login"]) && isset($_POST["password"]) && null != $_POST["login"] && null != $_POST["password"]) {

        if ($usuario->login($_POST["login"], $_POST["password"])) {
            array_push($msgLogin, array("status" => "success", "msg" => "Logado com sucesso! <br/> Aguarde o redirecionamento!"));
            echo '<meta http-equiv="refresh" content="0;URL=/admin">';
        } else {
            array_push($msgLogin, array("status" => "danger", "msg" => "Usuário ou senha inválidas"));
        }
    } else {
        array_push($msgLogin, array("status" => "danger", "msg" => "Campos obrigatorios em branco!"));
    }
}
?>

<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, maximum-scale=1">

        <title>VRCLIC - Web Design Studio</title>
        <link rel="icon" type="image/png" href="/res/site/layout/img/logoB_32.png" />

        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>

        <link href="/res/site/layout/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="/res/site/layout/css/style.css" rel="stylesheet" type="text/css">
        <link href="/res/site/layout/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="/res/site/layout/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="/res/site/layout/css/animate.css" rel="stylesheet" type="text/css">
<!--        <script src="contactform/contactform.js"></script>-->


    </head>
    <!--<php require 'header.php'; ?-->
    <body>


        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <br/>
                    <center>
                        <img src="/res/site/layout/img/logo_200.png" class="img-responsive img-fluid"/>
                    </center>
                    
                    <h3 class="text-center" style="margin-top: 10px; margin-bottom: 10px;">GERENCIAR SITE</h3>                               
                    
                    <?php
                    if (isset($msgLogin)) {
                        ErrorHandler::printErrorForm($msgLogin);
                    }
                    ?>

                    <form class="form-signin" action="/a/login" method="POST">
                        <div class="list-group">
                            <div class="list-group-item">
                                <input type="text" autofocus="autofocus" class="form-control" placeholder="Login" name="login">
                            </div>
                            <div class="list-group-item">
                                <input type="password" class="form-control" placeholder="Senha" name="password">
                            </div>
                            <div class="list-group-item">                                   
                                <button type="submit" class="btn btn-success btn-block">Acessar <i class="fa fa-angle-right"></i></button>
                            </div>                           
                        </div>
                    </form>                            
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>

    </body>
</html>