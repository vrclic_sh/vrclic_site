<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1">

    <title>VRCLIC - Soluções Digitais</title>
    <link rel="icon" type="image/png" href="/res/site/layout/img/logoB_32.png" />

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>

    <link href="/res/site/layout/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/res/site/layout/css/style.css" rel="stylesheet" type="text/css">
    <link href="/res/site/layout/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="/res/site/layout/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="/res/site/layout/css/animate.css" rel="stylesheet" type="text/css">

    <!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->
</head>

<body>
    <header class="header" id="header">
        <!--header-start-->
        <div class="container">
            <figure class="logo animated fadeInDown delay-07s">
                <a href="#"><img src="/res/site/layout/img/logoB_400.png" alt=""></a>
            </figure>
            <h1 class="animated fadeInDown delay-07s">Seja Bem Vindo!</h1>
            <ul class="we-create animated fadeInUp delay-1s">
                <li>Somos uma agência digital que adora criar projetos inovadores.</li>
            </ul>
            <a class="link animated fadeInUp delay-1s servicelink" href="#service">Vamos Lá!</a>
        </div>
    </header>
    <!--header-end-->

    <nav class="main-nav-outer" id="test">
        <!--main-nav-start-->
        <div class="container">
            <ul class="main-nav">
                <li><a href="#header">Home</a></li>
                <li><a href="#service">Serviços</a></li>
                <li><a href="#Portfolio">Portfólio</a></li>
                <li class="small-logo"><a href="#header"><img src="/res/site/layout/img/logo_144.png" alt=""></a></li>
                <li><a href="#about">Sobre</a></li>
                <li><a href="#team">Time</a></li>
                <li><a href="#contact">Contato</a></li>
            </ul>
            <a class="res-nav_click" href="#"><i class="fa-bars"></i></a>
        </div>
    </nav>
    <!--main-nav-end-->



    <section class="main-section" id="service">
        <!--main-section-start-->
        <div class="container">
            <h2>Serviços</h2>
            <h6>Oferecemos um serviço excepcional com abraços de cortesia.</h6>
            <div class="row">
                <div class="col-lg-4 col-sm-6 wow fadeInLeft delay-05s">
                    <div class="service-list">
                        <div class="service-list-col1">
                            <i class="fa-paw"></i>
                        </div>
                        <div class="service-list-col2">
                            <h3>Identidade Visual</h3>
                            <p>Construimos a sua marca de sucesso!</p>
                        </div>
                    </div>
                    <div class="service-list">
                        <div class="service-list-col1">
                            <i class="fa-gear"></i>
                        </div>
                        <div class="service-list-col2">
                            <h3>Desenvolvimento Web</h3>
                            <p>Projeto inovadores e com qualidade.</p>
                        </div>
                    </div>
                    <div class="service-list">
                        <div class="service-list-col1">
                            <i class="fa-android"></i>
                        </div>
                        <div class="service-list-col2">
                            <h3>Design Móvel</h3>
                            <p>Aplicativos desenvolvidos com as últimas tecnologias.</p>
                        </div>
                    </div>
                    <div class="service-list">
                        <div class="service-list-col1">
                            <i class="fa-medkit"></i>
                        </div>
                        <div class="service-list-col2">
                            <h3>Suporte 24/7</h3>
                            <p>Nosso trabalho não para, estamos sempre ativos.</p>
                        </div>
                    </div>
                </div>
                <figure class="col-lg-8 col-sm-6  text-right wow fadeInUp delay-02s">
                    <img src="/res/site/layout/img/macbook-pro.png" alt="" />
                </figure>
            </div>
        </div>
    </section>
    <!--main-section-end-->



    <section class="main-section alabaster">
        <!--main-section alabaster-start -->
        <div class="container">
            <div class="row">
                <figure class="col-lg-5 col-sm-4 wow fadeInLeft">
                    <img src="/res/site/layout/img/android.png" alt="">
                </figure>
                <div class="col-lg-7 col-sm-8 featured-work">
                    <h2>Trabalhamos com muito Esmero</h2>
                    <p class="padding-b">Buscamos em cada projeto ou trabalho trazer um alto grau de excelência, trazendo os melhores resultados, elegância, asseio e qualidade.</P>
                    <div class="featured-box">
                        <div class="featured-box-col1 wow fadeInRight delay-02s">
                            <i class="fa-magic"></i>
                        </div>
                        <div class="featured-box-col2 wow fadeInRight delay-02s">
                            <h3>Magia no Desenvolvimento do Layouts</h3>
                            <p>Realizamos estudos com a identidade da empresa, com isso conseguimos construir layouts que expressam e trazem para o digital a verdadeira essência do negócio.</p>
                        </div>
                    </div>
                    <div class="featured-box">
                        <div class="featured-box-col1 wow fadeInRight delay-04s">
                            <i class="fa-gift"></i>
                        </div>
                        <div class="featured-box-col2 wow fadeInRight delay-04s">
                            <h3>Cuidadosamente Embalados e Entregues</h3>
                            <p>Quando desenvolvemos um projeto, garantimos que os mesmos então sendo embalados com as mais novas tecnologias e sendo entregues com conceitos de escalabilidade.</p>
                        </div>
                    </div>
                    <div class="featured-box">
                        <div class="featured-box-col1 wow fadeInRight delay-06s">
                            <i class="fa-dashboard"></i>
                        </div>
                        <div class="featured-box-col2 wow fadeInRight delay-06s">
                            <h3>SEO Otimizado</h3>
                            <p>Utilizamos ferramentas de otimização, garantindo rapidez e indexação nos mais variados meios de pesquisa e compartilhamentos.</p>
                        </div>
                    </div>
                    <a class="Learn-More" href="#">Learn More</a>
                </div>
            </div>
        </div>
    </section><!-- main-section alabaster-end -->


    <section class="main-section paddind" id="Portfolio">
        <!--main-section-start-->
        <div class="container">
            <h2>Portfólio</h2>
            <h6>Nosso novo portfólio, você vai querer que façamos o seu projeto!.</h6>
            <div class="portfolioFilter">
                <?php
                $grupo = new GrupoPortfolio();
                ?>
                <ul class="Portfolio-nav wow fadeIn delay-02s">
                    <li><a href="#" data-filter="*" class="current">Tudo</a></li>
                    <?php foreach ($grupo->getAllAtivas() as $item) { ?>
                        <li><a href="#" data-filter=".cod<?php echo $item["id"] ?>"><?php echo $item["titulo"] ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="portfolioContainer wow fadeInUp delay-04s">
            <?php
            $portfolio = new Portfolio();
            foreach ($portfolio->getAll() as $value) {
            ?>
                <div class="Portfolio-box cod<?php echo $value['tb_grupo_portfolio_id'] ?>">
                    <img src="/img/<?php echo $value["token_img"] ?>/300/1" alt="" style="border: 1px solid #CCCCCC; background-color: transparent" />
                    <h3><?php echo $value['nome'] ?></h3>
                    <?php if (empty($value['link'])) { ?>

                    <?php } else { ?>
                        <a href="<?php echo $value['link'] ?>" class="btn btn-success" title="Visualizar" target="_blank"><i class="fa fa-eye"></i> Ver Projeto</a>
                    <?php } ?>
                </div>
            <?php
            }
            ?>
        </div>
    </section>
    <!--main-section-end-->



    <section class="main-section client-part" id="about">
        <!--main-section client-part-start-->
        <div class="container">
            <b class="quote-right wow fadeInDown delay-03"><i class="fa fa-quote-right"></i></b>
            <div class="row">
                <div class="col-lg-12">
                    <p class="client-part-haead wow fadeInDown delay-05" style="text-align: justify">
                        Estamos aqui desde 2010 para lhe oferecer uma opção séria e confiável de trabalho e aplicações on-line!<br /><br />

                        Cada um de nossos projetos é desenvolvido para o futuro, visando a garantia de expansão e crescimento exponencial.<br /><br />

                        Com o compromisso de desenvolver projetos com as mais novas tecnologias e tendências, estamos sempre colocando as experiências e novos aprendizados a toda prova para elevar a qualidade das soluções.<br /><br />

                        Temos expertise em negócio, podemos melhorar processos e agilizar métodos de trabalho.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--main-section client-part-end-->
    <div class="c-logo-part">
        <!--c-logo-part-start-->
        <div class="container">
            <ul>
                <li><a href="https://www.hostinger.com.br/" target="_blank"><img src="/res/site/layout/img/c-liogo1.png" alt="" /></a></li>
                <li><a href="https://getbootstrap.com/" target="_blank"><img src="/res/site/layout/img/c-liogo2.png" alt="" /></a></li>
                <li><a href="https://trello.com/" target="_blank"><img src="/res/site/layout/img/c-liogo3.png" alt="" /></a></li>
                <li><a href="https://bitbucket.org/product" target="_blank"><img src="/res/site/layout/img/c-liogo4.png" alt="" /></a></li>
                <li><a href="#" target="_blank"><img src="/res/site/layout/img/c-liogo5.png" alt="" /></a></li>
            </ul>
        </div>
    </div>

    <div class="c-logo-part-jooble">
        <!--c-logo-part-start-->
        <div class="container">
            <ul>
                <li>
                    <a href="https://br.jooble.org/vagas-de-emprego-analista-de-ti " target="_blank">
                        <img src="https://br.jooble.org/css/images/jooble_cvp.svg" alt="" class="img-fluid" style="max-width: 80px" /><br />
                        <small>Jooble é um site onde você pode encontrar<br /> empregos pelo mundo inteiro</small>

                    </a><br />
                    <h3><a href="https://br.jooble.org/vagas-de-emprego-analista-de-ti " target="_blank">Vagas para Analista de TI</a></h3>
                </li>
            </ul>
        </div>
    </div>
    <!--c-logo-part-end-->
    <section class="main-section team" id="team">
        <!--main-section team-start-->
        <div class="container">
            <h2>TIME</h2>
            <h6>Conheça nosso time, espertos e dedicados.</h6>
            <div class="team-leader-block clearfix">

                <?php
                $equipeObj = new Equipe();

                foreach ($equipeObj->getAll() as $equipe) {
                ?>

                    <div class="team-leader-box">
                        <div class="team-leader wow fadeInDown delay-03s">
                            <div class="team-leader-shadow">
                                <?php
                                if (isset($equipe['site']) && !empty($equipe['site'])) {
                                    echo '<a target="_blank" href="' . $equipe['url_pessoal'] . '" ></a>';
                                } else {
                                    echo '<a href="#"></a>';
                                }
                                ?>
                            </div>
                            <img src="/img/<?php echo $equipe["token_img"] ?>/400/1" class="img-responsive" alt="" />
                            <ul>
                                <?php
                                if (isset($equipe['facebook']) && !empty($equipe['facebook'])) {
                                    echo "<li><a href='" . $equipe["facebook"] . "' class='fa-facebook' target='_blank'></a></li>";
                                }
                                if (isset($equipe['linkedin']) && !empty($equipe['linkedin'])) {
                                    echo "<li><a href='" . $equipe["linkedin"] . "' class='fa-linkedin' target='_blank'></a></li>";
                                }
                                if (isset($equipe['instagram']) && !empty($equipe['instagram'])) {
                                    echo "<li><a href='" . $equipe["instagram"] . "' class='fa-instagram' target='_blank'></a></li>";
                                }
                                if (isset($equipe['twitter']) && !empty($equipe['twitter'])) {
                                    echo "<li><a href='" . $equipe["twitter"] . "' class='fa-twitter' target='_blank'></a></li>";
                                }
                                if (isset($equipe['googleplus']) && !empty($equipe['googleplus'])) {
                                    echo "<li><a href='" . $equipe["googleplus"] . "' class='fa-google-plus' target='_blank'></a></li>";
                                }
                                if (isset($equipe['url_pessoal']) && !empty($equipe['url_pessoal'])) {
                                    echo "<li><a href='" . $equipe["url_pessoal"] . "' class='fa-home' target='_blank'></a></li>";
                                }
                                ?>
                            </ul>
                        </div>
                        <h3 class="wow fadeInDown delay-03s"><b><?php echo $equipe['nome']; ?></b></h3>
                        <span class="wow fadeInDown delay-03s">
                            <?php echo $equipe['funcao']; ?><br />
                            <?php echo $equipe['whatsapp']; ?><br />
                            <?php echo $equipe['email']; ?>
                        </span>
                    </div>

                <?php
                }
                ?>

            </div>
        </div>
    </section>
    <!--main-section team-end-->



    <section class="business-talking">
        <!--business-talking-start-->
        <div class="container">
            <h2>Vamos Falar de Negócios.</h2>
        </div>
    </section>
    <!--business-talking-end-->

    <div class="container">
        <section class="main-section contact" id="contact">
            <?php
            // Fix Api Whatsapp on Desktops
            // Dev: Jean Livino
            // insert the text and message

            $phoneV = '+5545999638299';
            $messageV = 'Olá Vanderson, vim pelo contato do site VRCLIC, preciso falar sobre negócios!';

            $phoneM = '+5545988238608';
            $messageM = 'Olá Marcos, vim pelo contato do site VRCLIC, preciso falar sobre negócios!';

            // DO NOT EDIT BELOW
            $messageV = urlencode($messageV);
            $messageV = str_replace('+', '%20', $messageV);
            $messageM = urlencode($messageM);
            $messageM = str_replace('+', '%20', $messageM);
            $iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
            $android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
            $palmpre = strpos($_SERVER['HTTP_USER_AGENT'], "webOS");
            $berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
            $ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

            ?>
            <div class="row">
                <div class="col-lg-6 col-sm-7 wow fadeInLeft">
                    <div class="contact-info-box address clearfix">
                        <h3><i class="fa fa-map-marker " aria-hidden="true"></i> Local:</h3>
                        <span>Santa Helena / Cascavel<br>Paraná - Brasil.</span>
                    </div>
                    <hr />
                    <div class="contact-info-box phone clearfix">
                        <h3><i class="fa fa-mobile" aria-hidden="true"></i> Fone:</h3>
                        <span>
                            Vanderson: (45)99963-8299
                            <?php
                            if ($iphone || $android || $palmpre || $ipod || $berry == true) { ?>
                                <a target="_blank" class="btn btn-success btn-xs" href="whatsapp://send?phone=<?php echo $phoneV; ?>&text=<?php echo $messageV; ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> Chamar no WhatsApp</a>
                            <?php } else { ?>
                                <a target="_blank" class="btn btn-success btn-xs" href="https://web.whatsapp.com/send?phone=<?php echo $phoneV; ?>&text=<?php echo $messageV; ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> Chamar no WhatsApp</a>
                            <?php } ?>
                            <br />
                            Marcos: (45)98823-8608
                            <?php
                            if ($iphone || $android || $palmpre || $ipod || $berry == true) { ?>
                                <a target="_blank" class="btn btn-success btn-xs" href="whatsapp://send?phone=<?php echo $phoneM; ?>&text=<?php echo $messageM; ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> Chamar no WhatsApp</a>
                            <?php } else { ?>
                                <a target="_blank" class="btn btn-success btn-xs" href="https://web.whatsapp.com/send?phone=<?php echo $phoneM; ?>&text=<?php echo $messageM; ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> Chamar no WhatsApp</a>
                            <?php } ?>
                        </span>
                    </div>
                    <hr />
                    <div class="contact-info-box email clearfix">
                        <h3><i class="fa fa-envelope" aria-hidden="true"></i> E-mail:</h3>
                        <span>vanderson@vrclic.com.br<br />marcos@vrclic.com.br</span>
                    </div>
                    <hr />

                </div>
                <div class="col-lg-6 col-sm-5 wow fadeInUp delay-05s">
                    <h3><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Siga-nos!</h3>
                    <ul class="social-link">
                        <li class="facebook">
                            <a href="https://www.facebook.com/vrclic" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook da VRCLIC</a>
                        </li>

                        <li class="linkedin">
                            <a href="https://www.linkedin.com/company/vrclic/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin da VRCLIC</a>
                        </li>

                    </ul>
                    <!--
                        <div class="form">

                            <div id="sendmessage">Sua mensagem foi enviada. Obrigado!</div>
                            <div id="errormessage"></div>
                            <form action="/contato/home" method="POST" role="form" class="contactForm">
                                <div class="form-group">
                                    <input type="text" name="nome" class="form-control input-text" id="nome" placeholder="Seu Nome" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control input-text" name="email" id="email" placeholder="Seu E-mail" data-rule="email" data-msg="Please enter a valid email" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-text" name="telefone" id="telefone" placeholder="Seu Telefone" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control input-text text-area" name="conteudo" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Sua Mensagem"></textarea>
                                    <div class="validation"></div>
                                </div>

                                <div class="text-center"><button type="submit" class="input-btn">Enviar Mensagem</button></div>
                            </form>
                        </div>	
                        -->
                </div>
            </div>
        </section>
    </div>
    <footer class="footer">
        <div class="container">
            <div style="text-align: center"><a href="#"><img src="/res/site/layout/img/logoB_200.png" alt="" style="max-width: 100px"></a></div>
            <span class="copyright">&copy; Direitos Reservados - 2010/<?php echo date('Y') ?></span>
            <div class="credits">
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="/res/site/layout/js/jquery.1.8.3.min.js"></script>
    <script type="text/javascript" src="/res/site/layout/js/bootstrap.js"></script>
    <script type="text/javascript" src="/res/site/layout/js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="/res/site/layout/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="/res/site/layout/js/jquery.isotope.js"></script>
    <script type="text/javascript" src="/res/site/layout/js/wow.js"></script>
    <script type="text/javascript" src="/res/site/layout/js/classie.js"></script>
    <script type="text/javascript" src="contactform/contactform.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-15828906-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-15828906-1');
    </script>



    <script type="text/javascript">
        $(document).ready(function(e) {
            $('#test').scrollToFixed();
            $('.res-nav_click').click(function() {
                $('.main-nav').slideToggle();
                return false

            });

        });

        wow = new WOW({
            animateClass: 'animated',
            offset: 100
        });
        wow.init();


        $(window).load(function() {

            $('.main-nav li a, .servicelink').bind('click', function(event) {
                var $anchor = $(this);

                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top - 102
                }, 1500, 'easeInOutExpo');
                /*
                 if you don't want to use the easing effects:
                 $('html, body').stop().animate({
                 scrollTop: $($anchor.attr('href')).offset().top
                 }, 1000);
                 */
                if ($(window).width() < 768) {
                    $('.main-nav').hide();
                }
                event.preventDefault();
            });

            var $container = $('.portfolioContainer'),
                $body = $('body'),
                colW = 375,
                columns = null;


            $container.isotope({
                // disable window resizing
                resizable: true,
                masonry: {
                    columnWidth: colW
                }
            });

            $(window).smartresize(function() {
                // check if columns has changed
                var currentColumns = Math.floor(($body.width() - 30) / colW);
                if (currentColumns !== columns) {
                    // set new column count
                    columns = currentColumns;
                    // apply width to container manually, then trigger relayout
                    $container.width(columns * colW)
                        .isotope('reLayout');
                }

            }).smartresize(); // trigger resize to set container width
            $('.portfolioFilter a').click(function() {
                $('.portfolioFilter .current').removeClass('current');
                $(this).addClass('current');

                var selector = $(this).attr('data-filter');
                $container.isotope({

                    filter: selector,
                });
                return false;
            });

        });
    </script>

</body>

</html>