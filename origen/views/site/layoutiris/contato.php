<!DOCTYPE html>
<html lang="pt">
    <?php require 'header.php'; ?>
    <body>

        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <?php require 'include/menu.php'; ?>
        </header><!--/header-->

        <section id="single-page-slider" class="no-margin">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="center gap fade-down section-heading">
                                        <h2 class="main-title">CONTATO</h2>
                                        <hr>
                                        <p>Agilidade com Qualidade!</p>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div><!--/.item-->
                </div><!--/.carousel-inner-->
            </div><!--/.carousel-->
        </section><!--/#main-slider-->

        <div id="content-wrapper">
            <div id="mapwrapper">
                <div id="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1076.2667450010852!2d-54.33483019741366!3d-24.85990823743033!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe4736c0d7cb91d78!2sFoto+Iris!5e0!3m2!1spt-BR!2sbr!4v1542163764991" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <section id="contact" class="white">
                <div class="container">
                    <div class="gap"></div>
                    <div class="row">
                        <div class="col-md-4 fade-up">
                            <h3>informações de Contato</h3>
                            <p>
                                <span class="icon icon-home"></span>Avenida Brasil, 1535<br/>
                                <span class="icon icon-phone"></span>(45) 3268-1472 <br/>
                                <span class="icon icon-mobile"></span>(45) 99999-9999 <br/>
                                <span class="icon icon-envelop"></span> <a href="mailto:contato@fotoiris.com.br">contato@fotoiris.com.br</a> <br/>
<!--                                <span class="icon icon-twitter"></span> <a href="#">@infinityteam.com</a> <br/>-->
                                <span class="icon icon-facebook"></span> <a href="https://www.facebook.com/shfotoiris/" target="_blank">Foto Iris</a> <br/>
                            </p>
                        </div><!-- col -->

                        <div class="col-md-8 fade-up">
                            <h3>Deixe-nos uma Mensagem</h3>
                            <br>
                            <br>
                            <div id="message"></div>
                            <form method="post" action="sendemail.php" id="contactform">
                                <input type="text" name="name" id="name" placeholder="Nome" />
                                <input type="text" name="email" id="email" placeholder="E-mail" />
                                <input type="text" name="website" id="website" placeholder="Telefone" />
                                <textarea name="comments" id="comments" placeholder="Mensagem"></textarea>
                                <input class="btn btn-outlined btn-primary" type="submit" name="submit" value="Enviar Mensagem" />
                            </form>
                        </div><!-- col -->
                    </div><!-- row -->  
                    <div class="gap"></div>         
                </div>
            </section>
        </div>
        <?php require 'footer.php'; ?>
    </body>
</html>