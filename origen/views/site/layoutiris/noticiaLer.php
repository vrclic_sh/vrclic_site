<!DOCTYPE html>
<html lang="pt">
    <?php require 'header.php'; ?>
    <body>

        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <?php require 'include/menu.php'; ?>
        </header><!--/header-->

        <section id="single-page-slider" class="no-margin">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="center gap fade-down section-heading">
                                        <h2 class="main-title">NOTÍCIAS</h2>
                                        <hr>
                                        <p>NOVIDADES NO MUNDO DA FOTOGRAFIA.</p>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div><!--/.item-->
                </div><!--/.carousel-inner-->
            </div><!--/.carousel-->
        </section><!--/#main-slider-->

        <div id="content-wrapper">
            <section id="blog" class="white">
                <div class="container">            
                    <div class="gap"></div>
                    <div class="row">                                
                        <div class="col-md-12">
                            <div class="blog">
                                <div class="blog-item">
                                    <div class="blog-featured-image">
                                        <img class="img-responsive img-blog" src="https://abrilexame.files.wordpress.com/2018/10/gettyimages-1052757074.jpg?quality=70&strip=info&resize=680,453" alt="" />                                         
                                    </div> 
                                    <div class="blog-content">
                                        <h3 class="main-title">Foto de jovem palestino em protesto na Faixa de Gaza viraliza. Entenda</h3>
                                        <div class="entry-meta">                                            
                                            <span><i class="fa fa-clock-o"></i> 10/11/2018</span>                                            
                                        </div>
                                        <p class="lead">Foto de jovem manifestante registrada no início da semana na Faixa de Gaza foi comparada com obra do francês Eugène Delacroix. </p>

                                        <p>
                                            São Paulo – Com o torso nu, um estilingue numa mão e uma bandeira Palestina na outra, a foto de um jovem palestino protestando na Faixa de Gaza viralizou nas redes sociais mundo afora nesta semana.
                                            <br/><br/>
                                            A imagem ganhou comparações com a história icônica dos personagens Davi e Golias, bem como com a obra de arte “A Liberdade Guiando o Povo”, do pintor francês Eugène Delacroix. Houve, ainda, quem achasse que a imagem seria uma espécie de celebração da violência.
                                            <br/><br/>
                                            De acordo com a rede de notícias Al Jazeera, a imagem foi registrada no último dia 22 de outubro pelo fotógrafo Mustafa Hassouna, para a rede de notícias turca Andalou. Nela, está o jovem A’ed Abu Amro, 20 anos, em meio a uma fumaça densa que parece engolir um protesto na Faixa de Gaza contra bloqueios impostos por Israel.
                                        </p>
                                        <hr>

                                        <ul class="portfolio-items col-3 isotope fade-up">
                                            <li class="portfolio-item">
                                                <div class="item-inner">
                                                    <img src="http://placehold.it/800x600" alt="">                                                                                  
                                                    <div class="overlay">
                                                        <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>             
                                                    </div>           
                                                </div>           
                                            </li>

                                            <li class="portfolio-item">
                                                <div class="item-inner">
                                                    <img src="http://placehold.it/800x600" alt="">                                                                                  
                                                    <div class="overlay">
                                                        <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>             
                                                    </div>           
                                                </div>           
                                            </li>

                                            <li class="portfolio-item">
                                                <div class="item-inner">
                                                    <img src="http://placehold.it/800x600" alt="">                                                                                  
                                                    <div class="overlay">
                                                        <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>             
                                                    </div>           
                                                </div>           
                                            </li>

                                            <li class="portfolio-item">
                                                <div class="item-inner">
                                                    <img src="http://placehold.it/800x600" alt="">                                                                                  
                                                    <div class="overlay">
                                                        <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>             
                                                    </div>           
                                                </div>           
                                            </li>

                                            <li class="portfolio-item">
                                                <div class="item-inner">
                                                    <img src="http://placehold.it/800x600" alt="">                                                                                  
                                                    <div class="overlay">
                                                        <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>             
                                                    </div>           
                                                </div>           
                                            </li>

                                            <li class="portfolio-item">
                                                <div class="item-inner">
                                                    <img src="http://placehold.it/800x600" alt="">                                                                                  
                                                    <div class="overlay">
                                                        <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>             
                                                    </div>           
                                                </div>           
                                            </li>

                                            <li class="portfolio-item">
                                                <div class="item-inner">
                                                    <img src="http://placehold.it/800x600" alt="">                                                                                  
                                                    <div class="overlay">
                                                        <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>             
                                                    </div>           
                                                </div>           
                                            </li>
                                        </ul>
                                        <hr>
                                        <!--/#comments-->
                                    </div>
                                </div>                                
                            </div>
                        </div><!--/.col-md-8-->
                    </div><!--/.row-->
                    
                    <div class="row">
                        <aside class="col-md-12"> 

                            <div class="widget categories">
                                <h3 class="widget-title">Mais Notícias</h3>
                                <div class="row">
                                    <div class="author well">
                                        <a href="#">
                                            <div class="media">
                                                <div class="pull-left">
                                                    <img class="avatar img-thumbnail author-box-image" src="https://abrilexame.files.wordpress.com/2018/02/selfie.jpg?quality=70&strip=info&resize=680,453" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="media-heading">
                                                        <strong>Cultura da selfie leva museus a autorizar fotografias</strong>
                                                    </div>                                                
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="media">
                                                <div class="pull-left">
                                                    <img class="avatar img-thumbnail author-box-image" src="https://abrilexame.files.wordpress.com/2018/10/gettyimages-1052757074.jpg?quality=70&strip=info&resize=680,453" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="media-heading">
                                                        <strong>Foto de jovem palestino em protesto na Faixa de Gaza viraliza. Entenda</strong>
                                                    </div>                                                
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="media">
                                                <div class="pull-left">
                                                    <img class="avatar img-thumbnail author-box-image" src="https://abrilexame.files.wordpress.com/2018/02/selfie.jpg?quality=70&strip=info&resize=680,453" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="media-heading">
                                                        <strong>Cultura da selfie leva museus a autorizar fotografias</strong>
                                                    </div>                                                
                                                </div>
                                            </div>
                                        </a>

                                        <a href="#">
                                            <div class="media">
                                                <div class="pull-left">
                                                    <img class="avatar img-thumbnail author-box-image" src="https://abrilexame.files.wordpress.com/2018/10/gettyimages-1052757074.jpg?quality=70&strip=info&resize=680,453" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="media-heading">
                                                        <strong>Foto de jovem palestino em protesto na Faixa de Gaza viraliza. Entenda</strong>
                                                    </div>                                                
                                                </div>
                                            </div>
                                        </a>
                                    </div><!--/.author-->
                                </div>                     
                            </div><!--/.categories-->                            
                        </aside>
                    </div>
                </div>
            </section><!--/#blog-->


        </div>

        <?php require 'footer.php'; ?>

    </body>
</html>