
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Foto Iris - Laboratório Digital</title>
    <link href="../../../res/site/layoutiris/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../res/site/layoutiris/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../../res/site/layoutiris/css/pe-icons.css" rel="stylesheet">
    <link href="../../../res/site/layoutiris/css/prettyPhoto.css" rel="stylesheet">
    <link href="../../../res/site/layoutiris/css/animate.css" rel="stylesheet">
    <link href="../../../res/site/layoutiris/css/style.css" rel="stylesheet">
    <script src="../../../res/site/layoutiris/js/jquery.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="../../../res/site/layoutiris/images/ico/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="../../../res/site/layoutiris/images/ico/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../../../res/site/layoutiris/images/ico/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../../../res/site/layoutiris/images/ico/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" href="../../../res/site/layoutiris/images/ico/apple-touch-icon-57x57.png">

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            'use strict';
            jQuery('body').backstretch([
                "../../../res/site/layoutiris/images/bg/bg1.jpg",
                "../../../res/site/layoutiris/images/bg/bg2.jpg",
                "../../../res/site/layoutiris/images/bg/bg3.jpg"
            ], {duration: 5000, fade: 500, centeredY: true});

            /*$("#mapwrapper").gMap({ controls: false,
             scrollwheel: false,
             markers: [{ 	
             latitude:-24.860010,
             longitude: -54.334659,
             icon: { image: "../../../res/site/layoutiris/images/marker.png",
             iconsize: [44,44],
             iconanchor: [12,46],
             infowindowanchor: [12, 0] } }],
             icon: { 
             image: "../../../res/site/layoutiris/images/marker.png", 
             iconsize: [26, 46],
             iconanchor: [12, 46],
             infowindowanchor: [12, 0] },
             latitude:-24.860010,
             longitude: -54.334659,
             zoom: 14 });*/
        });

        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
    </script>
</head><!--/head-->