<div id="footer-wrapper">
        <section id="bottom" class="">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 about-us-widget">
                        <h4>Contato</h4>
                        <address>
                            <strong>Avenida Brasil, 1535</strong><br>
                            Santa Helena - PR<br/>
                            (45) 3268-1472<br/>
                            (45) 99999-9999<br/>
                            <a href="mailto:contato@fotoiris.com.br">contato@fotoiris.com.br</a> 
                        </address>
                    </div> <!--/.col-md-3-->
                    
                    <div class="col-md-3 col-sm-6">
                        <h4>Últimos Eventos</h4>
                        <div>
                            <div class="media">
                                <div class="pull-left">
                                    <img class="widget-img" src="../../../res/site/layoutiris/images/portfolio/folio01.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <span class="media-heading"><a href="#">Casamento de Fulano com Fulana</a></span>                                    
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img class="widget-img" src="../../../res/site/layoutiris/images/portfolio/folio02.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <span class="media-heading"><a href="#">Aniversário de 15 anos de Fulana</a></span>
                                </div>
                            </div>
                        </div>  
                    </div><!--/.col-md-3-->
                    
                    <div class="col-md-3 col-sm-6">
                        <h4>Sobre a Foto Iris</h4>
                        <p>Há 20 anos no ramo da fotografia e de cobertura de eventos, o Foto Iris se destaca pela qualidade e pelo bom atendimento aos clientes.</p>
                    </div><!--/.col-md-3-->

                    <div class="col-md-3 col-sm-6">
                        <h4>Revelar Fotos</h4>
                        <p>Envie suas fotos, escolha os tamanhos, realize o pagamento, depois venha retira-las.</p>
                        <a class="btn btn-outlined btnf btn-primary" href="uploadFotos.php">Enviar Fotos</a>
                    </div><!--/.col-md-3-->

                    

                    
                </div>
            </div>
        </section><!--/#bottom-->

        <footer id="footer" class="">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        &copy; <?php echo date('Y');?> - <a target="_blank" href="http://vrclic.com.br" title="VRCLIC Web Design Studio">VRCLIC</a> - Direitos Reservados.
                    </div>
                    <div class="col-sm-6">
                        <ul class="pull-right">
                            <li><a id="gototop" class="gototop" href="#"><i class="fa fa-chevron-up"></i></a></li><!--#gototop-->
                        </ul>
                    </div>
                </div>
            </div>
        </footer><!--/#footer-->
    </div>


    <script src="../../../res/site/layoutiris/js/plugins.js"></script>
    <script src="../../../res/site/layoutiris/js/bootstrap.min.js"></script>
    <script src="../../../res/site/layoutiris/js/jquery.prettyPhoto.js"></script>
    <script src="../../../res/site/layoutiris/js/jquery.isotope.min.js"></script>    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWDPCiH080dNCTYC-uprmLOn2mt2BMSUk&amp;sensor=true"></script> 
    <script src="../../../res/site/layoutiris/js/init.js"></script>