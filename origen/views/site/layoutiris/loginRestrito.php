<!DOCTYPE html>
<html lang="pt">
    <?php require 'header.php'; ?>
    <body>

        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <?php require 'include/menu.php'; ?>
        </header><!--/header-->

        <section id="single-page-slider" class="no-margin">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="center gap fade-down section-heading">
                                        <h2 class="main-title">ÁREA RESTRITA</h2>
                                        <hr>
                                        <p>GERENCIAR MINHAS FOTOS</p>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div><!--/.item-->
                </div><!--/.carousel-inner-->
            </div><!--/.carousel-->
        </section><!--/#main-slider-->

        <div id="content-wrapper">
            <section id="blog" class="white">
                <div class="container">                               
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-12"></div>
                            <div class="col-md-4 col-sm-12">
                                <h3>GERENCIAR MINHAS FOTOS</h3>
                                <form method="post" action="#" id="contactform">                                
                                    <input type="text" name="email" id="email" placeholder="E-mail" />
                                    <input type="password" name="passw" id="website" placeholder="Senha" />                                
                                    <input class="btn btn-outlined btn-primary btn-block" type="submit" name="submit" value="Acessar" />
                                </form>
                                <br/>

                                <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target=".recupSenha">RECUPERAR SENHA</button>

                                <div class="modal fade recupSenha fa-remove" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="exampleModalLabel">Recuperar Senha</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <label for="recipient-name" class="control-label">Seu E-mail:</label>
                                                    <input type="text" class="form-control" placeholder="Digite seu email para recuperar sua senha"/>
                                                </form>
                                            </div>
                                            <div class="modal-footer">                                                
                                                <button type="button" class="btn btn-primary btn-outlined">Recuperar Senha</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button type="button" class="btn btn-default btn-sm" style="float: right" data-toggle="modal" data-target="#cadastro">CADASTRAR-SE</button>

                                <div class="modal fade" id="cadastro" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="exampleModalLabel">...</h4>
                                            </div>
                                            <div class="modal-body">
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                <button type="button" class="btn btn-primary btn-outlined">Send message</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-12"></div>
                        </div>                                                
                    </div><!--/.row-->
                    <hr/>
                    <div class="row">
                        <div class="col-md-6 col-sm-12" style="text-align: right">
                            <h5>GERENCIAR REVELAÇÕES</h5>
                            * Envie Fotos para revelação.<br/>
                            * Escolha os tamanhos desejados<br/>
                            * Defina a Quantidade de cada Foto revelada.<br/>
                            * Pague por suas revelações<br/>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h5>FOTOS DE EVENTOS</h5>
                            * Escolha as fotos do seu evento.<br/>
                            * Escolha os tamanhos desejados<br/>
                            * Defina a Quantidade de cada Foto revelada.<br/>
                            * Pague por suas revelações<br/>
                        </div>
                    </div>

                </div>
            </section><!--/#blog-->
        </div>

        <?php require 'footer.php'; ?>

    </body>
</html>