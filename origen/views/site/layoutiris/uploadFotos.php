<!DOCTYPE html>
<html lang="pt">
    <?php require 'header.php'; ?>
    <body>

        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <?php require 'include/menu.php'; ?>
        </header><!--/header-->

        <section id="single-page-slider" class="no-margin">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="center gap fade-down section-heading">
                                        <h2 class="main-title">REVELAÇÃO DE FOTOS</h2>
                                        <hr>
                                        <p>ENVIE, ESCOLHA E REVELE SUAS FOTOS</p>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div><!--/.item-->
                </div><!--/.carousel-inner-->
            </div><!--/.carousel-->
        </section><!--/#main-slider-->

        <div id="content-wrapper">
            <section id="blog" class="white">
                <div class="container">                               
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h3>ENVIAR MINHAS FOTOS</h3>
                            <br/>
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab1" data-toggle="tab">Tamanho das Fotos</a></li>
                                <li><a href="#tab2" data-toggle="tab">Tipo do Papel</a></li>
                                <li><a href="#tab3" data-toggle="tab">Acabamento</a></li>
                                <li><a href="#tab4" data-toggle="tab">Correções</a></li>
                                <li><a href="#tab5" data-toggle="tab">Enviar Fotos</a></li>
                                <li><a href="#tab6" data-toggle="tab">Conferência</a></li>
                                <li><a href="#tab7" data-toggle="tab">Pagamento</a></li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <div class="container">
                                        <br/>
                                        <div class="alert alert-warning center">
                                            <h4>REVELAÇÃO DE FOTOS</h4>
                                            Papel Profissional com Qualidade Fotográfica
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="thumbnail">                                                                                                        
                                                    <div class="caption center">
                                                        <h3>10x15cm</h3>
                                                        <img src="http://algarve-portal.com/_wf/image/?/imgs/uploads/Ilha%20de%20Faro%20Strand%20mit%20Brandung.jpg&150x100"/>
                                                        <br/><br/><b style="font-size: 1.2em">R$ 0,90</b> cada<br/>
                                                        <a href="#" class="btn btn-outlined btn-primary">Revelar Este</a>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="thumbnail">                                                                                                    
                                                    <div class="caption center">
                                                        <h3>13x18cm</h3>
                                                        <img src="http://www.taipus.net/marau/images/thumbs/pontadomuta1292004264.jpg"/>
                                                        <br/><br/><b style="font-size: 1.2em">R$ 1,60</b> cada<br/>
                                                        <a href="#" class="btn btn-outlined btn-primary">Revelar Este</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="thumbnail">                                                
                                                    <div class="caption center">
                                                        <h3>15x21cm</h3>
                                                        <img src="http://www.sagres.net/images/praias/tonel-small.jpg"/>
                                                        <br/><br/><b style="font-size: 1.2em">R$ 3,90</b> cada<br/>
                                                        <a href="#" class="btn btn-outlined btn-primary">Revelar Este</a>
                                                    </div>
                                                </div>                                               
                                            </div>
                                        </div>


                                        <br/>
                                        <div class="alert alert-warning overlay center">
                                            <h4>AMPLIAÇÃO E PÔSTERS</h4>
                                            Escolha o tamanho ideal
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="thumbnail">                                                                                                   
                                                    <div class="caption center">
                                                        <h3>20x30cm</h3>
                                                        <img src="http://s1.1zoom.me/b7366/USA_Coast_Sunrises_and_sunsets_Scenery_Stones_Sky_541859_200x300.jpg"/>
                                                        <br/><br/><b style="font-size: 1.2em">R$ 12,90</b> cada<br/>
                                                        <a href="#" class="btn btn-outlined btn-primary">Revelar Este</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="thumbnail">                                                                                                   
                                                    <div class="caption center">
                                                        <h3>30x45cm</h3>
                                                        <img src="https://i.pinimg.com/originals/62/47/d4/6247d4015afed935f068387526220450.jpg"/>
                                                        <br/><br/><b style="font-size: 1.2em">R$ 45,90</b> cada<br/>
                                                        <a href="#" class="btn btn-outlined btn-primary">Revelar Este</a>
                                                    </div>
                                                </div>                                                                                               
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="thumbnail">                                                                                                   
                                                    <div class="caption center">
                                                        <h3>40x60cm</h3>
                                                        <img src="https://photos.smugmug.com/PAISAGENS/i-zbthBcS/0/8f856cde/L/_D7A0743_entre%20morros-L.jpg"/>
                                                        <br/><br/><b style="font-size: 1.2em">R$ 59,00</b> cada<br/>
                                                        <a href="#" class="btn btn-outlined btn-primary">Revelar Este</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="thumbnail">                                                                                                   
                                                    <div class="caption center">
                                                        <h3>50x75cm</h3>
                                                        <img src="https://66.media.tumblr.com/52bfd6c01bc6fe0f184d13edf7dea498/tumblr_p9eo7mwGgM1u8wonlo1_500.jpg"/>
                                                        <br/><br/><b style="font-size: 1.2em">R$ 83,90</b> cada<br/>
                                                        <a href="#" class="btn btn-outlined btn-primary">Revelar Este</a>
                                                    </div>
                                                </div>                                                                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br/>
                                            <div class="alert alert-warning center">
                                                <h4>SELECIONE O TIPO DE PAPEL</h4>                                            
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4>Fotográfico Premium</h4>
                                                    Altíssima qualidade produzido em Papel Fotográfico Profissional, cores mais vivas e papel mais grosso.<br/><br/>
                                                    <a href="#" class="btn btn-outlined btn-primary">Selecionar</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4>Fotográfico</h4>
                                                    Revelação de fotos com nossa tradicional qualidade de imagem e preço incrível!<br/><br/>
                                                    <a href="#" class="btn btn-outlined btn-primary">Selecionar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br/>
                                            <div class="alert alert-warning center">
                                                <h4>ESCOLHA SEU ACABAMENTO PREFERIDO</h4> 
                                                Você prefere papel sem Brilho ou com Brilho?
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4>Fosco</h4>
                                                    Todas as suas Fotos serão Reveladas em Papel Fosco (Sem Brilho)<br/>
                                                    <br/>
                                                    O papel fosco possui acabamento diferenciado, com superfície levemente perolada e com menos reflexo. Este tipo de acabamento é muito usado em revelação de fotografias profissionais.<br/>
                                                    <br/>
                                                    <a href="#" class="btn btn-outlined btn-primary">Selecionar</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4>Brilho</h4>
                                                    Todas as suas Fotos serão Reveladas em Papel Brilhante (Com Brilho)<br/>
                                                    <br/>
                                                    O papel com brilho possui uma camada envernizada com reflexo, dando impressão de cores brilhantes e mais vivas.<br/><br/>

                                                    <a href="#" class="btn btn-outlined btn-primary">Selecionar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br/>
                                            <div class="alert alert-warning center">
                                                <h4>CORREÇÃO</h4> 
                                                O que significa utilizar a Correção?
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4>Com Correção</h4>
                                                    Ao ativar a opção Correção da Imagem, a nitidez, a cor, o contraste e os olhos vermelhos das suas fotos digitais serão corrigidos. 
                                                    Essa opção realçará a sua foto, o resultado é excelente.<br/> 
                                                    Se suas fotos passaram por algum programa para edição de Imagem, desabilite a Correção.<br/><br/>
                                                    <a href="#" class="btn btn-outlined btn-primary">Selecionar</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4>Sem Correção</h4>
                                                    Ao manter a opção Correção desativada, não aplicaremos nenhum ajuste em suas fotos.<br/> 
                                                    Caso as suas fotos, já tenham passado por algum tratamento de imagem (redução de brilho, olhos vermelhos, etc., desabilite a Correção.<br/><br/>
                                                    <a href="#" class="btn btn-outlined btn-primary">Selecionar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab5">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br/>
                                            <div class="alert alert-warning center">
                                                <h4>ENVIAR SUAS FOTOS</h4>
                                                Carregue suas Fotos!
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Enviar Fotos:</label>
                                                    <form>
                                                        <input type="file" multiple="multiple" class="form-control" placeholder="Selecionara minhas Fotos"/>
                                                    </form>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="alert alert-warning center">
                                                <h4>FOTOS ENVIADAS</h4>
                                                Selecione o tamanho indivual, Selecione as quantidades!                                        
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">                                            
                                                    <form>
                                                        <div class="col-md-3">
                                                            <div class="thumbnail">                                                                                                   
                                                                <div class="caption">                                                    
                                                                    <img src="../../../res/site/layoutiris/images/portfolio/folio02.jpg" class="img-responsive"/>
                                                                    <p class="small">
<!--                                                                        <i class="fa fa-circle" style="color: #00a65b" title="Imagem com Qualidade"></i> - -->
                                                                        titulodafoto.jpg
                                                                    </p>
                                                                    <label class="small">Tamanho:</label>
                                                                    <select class="form-control">
                                                                        <option>10x15</option>
                                                                        <option>13x18</option>
                                                                        <option>15x21</option>
                                                                        <option>20x30</option>
                                                                        <option>30x45</option>
                                                                        <option>40x60</option>
                                                                        <option>50x75</option>
                                                                    </select>
                                                                    <label class="small">Quantidade:</label>
                                                                    <input type="number" value="1" class="form-control"/>
                                                                </div>
                                                            </div>                                                                                               
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="thumbnail">                                                                                                   
                                                                <div class="caption">                                                    
                                                                    <img src="../../../res/site/layoutiris/images/portfolio/folio03.jpg" class="img-responsive"/>
                                                                    <p class="small">
<!--                                                                        <i class="fa fa-circle" style="color: #ac2925" title="Imagem sem Qualidade"></i> - -->
                                                                        titulodafoto.jpg
                                                                    </p>
                                                                    <label class="small">Tamanho:</label>
                                                                    <select class="form-control">
                                                                        <option>10x15</option>
                                                                        <option>13x18</option>
                                                                        <option>15x21</option>
                                                                        <option>20x30</option>
                                                                        <option>30x45</option>
                                                                        <option>40x60</option>
                                                                        <option>50x75</option>
                                                                    </select>
                                                                    <label class="small">Quantidade:</label>
                                                                    <input type="number" value="1" class="form-control"/>                                                    
                                                                </div>
                                                            </div>                                                                                               
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="thumbnail">                                                                                                   
                                                                <div class="caption">                                                    
                                                                    <img src="../../../res/site/layoutiris/images/portfolio/folio04.jpg" class="img-responsive"/>
                                                                    <p class="small">
<!--                                                                        <i class="fa fa-circle" style="color: #ec971f" title="Imagem com pouca Qualidade"></i> - -->
                                                                        titulodafoto.jpg
                                                                    </p>
                                                                    <label class="small">Tamanho:</label>
                                                                    <select class="form-control">
                                                                        <option>10x15</option>
                                                                        <option>13x18</option>
                                                                        <option>15x21</option>
                                                                        <option>20x30</option>
                                                                        <option>30x45</option>
                                                                        <option>40x60</option>
                                                                        <option>50x75</option>
                                                                    </select>
                                                                    <label class="small">Quantidade:</label>
                                                                    <input type="number" value="1" class="form-control"/>                                                    
                                                                </div>
                                                            </div>                                                                                               
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="thumbnail">                                                                                                   
                                                                <div class="caption">                                                    
                                                                    <img src="../../../res/site/layoutiris/images/portfolio/folio05.jpg" class="img-responsive"/>
                                                                    <p class="small">
<!--                                                                        <i class="fa fa-circle" style="color: #00a65b" title="Imagem com Qualidade"></i> - -->
                                                                        titulodafoto.jpg
                                                                    </p>
                                                                    <label class="small">Tamanho:</label>
                                                                    <select class="form-control">
                                                                        <option>10x15</option>
                                                                        <option>13x18</option>
                                                                        <option>15x21</option>
                                                                        <option>20x30</option>
                                                                        <option>30x45</option>
                                                                        <option>40x60</option>
                                                                        <option>50x75</option>
                                                                    </select>
                                                                    <label class="small">Quantidade:</label>
                                                                    <input type="number" value="1" class="form-control"/>                                                    
                                                                </div>
                                                            </div>                                                                                               
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="thumbnail">                                                                                                   
                                                                <div class="caption">                                                    
                                                                    <img src="../../../res/site/layoutiris/images/portfolio/folio06.jpg" class="img-responsive"/>
                                                                    <p class="small">
<!--                                                                        <i class="fa fa-circle" style="color: #ec971f" title="Imagem com pouca Qualidade"></i> - -->
                                                                        titulodafoto.jpg
                                                                    </p>
                                                                    <label class="small">Tamanho:</label>
                                                                    <select class="form-control">
                                                                        <option>10x15</option>
                                                                        <option>13x18</option>
                                                                        <option>15x21</option>
                                                                        <option>20x30</option>
                                                                        <option>30x45</option>
                                                                        <option>40x60</option>
                                                                        <option>50x75</option>
                                                                    </select>
                                                                    <label class="small">Quantidade:</label>
                                                                    <input type="number" value="1" class="form-control"/>                                                    
                                                                </div>
                                                            </div>                                                                                               
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="thumbnail">                                                                                                   
                                                                <div class="caption">                                                    
                                                                    <img src="../../../res/site/layoutiris/images/portfolio/folio07.jpg" class="img-responsive"/>
                                                                    <p class="small">
<!--                                                                        <i class="fa fa-circle" style="color: #00a65b" title="Imagem com Qualidade"></i> - -->
                                                                        titulodafoto.jpg
                                                                    </p>
                                                                    <label class="small">Tamanho:</label>
                                                                    <select class="form-control">
                                                                        <option>10x15</option>
                                                                        <option>13x18</option>
                                                                        <option>15x21</option>
                                                                        <option>20x30</option>
                                                                        <option>30x45</option>
                                                                        <option>40x60</option>
                                                                        <option>50x75</option>
                                                                    </select>
                                                                    <label class="small">Quantidade:</label>
                                                                    <input type="number" value="1" class="form-control"/>                                                    
                                                                </div>
                                                            </div>                                                                                               
                                                        </div>
                                                    </form>

                                                </div>

                                                <div class="col-md-12">
<!--                                                    <b><i class="fa fa-circle" style="color: #00a65b"></i> - Imagem com Qualidade</b> (Sua revelação ficará ótima).<br/> 
                                                    <b><i class="fa fa-circle" style="color: #ec971f"></i> - Imagem com pouca Qualidade</b> (Talvez sua revelação não fique tão boa).<br/> 
                                                    <b><i class="fa fa-circle" style="color: #ac2925"></i> - Imagem sem Qualidade</b> (Está imagem irá comprometer a qualidade da sua revelação).-->
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br/>
                                            <div class="alert alert-warning center">
                                                <h4>VALIDAR SUAS ESCOLHAS</h4>
                                                Vamos conferir se está tudo correto?
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Tamanho do Papel:</label>
                                                    <span style="font-size: 1.2em">10x15</span>
                                                    <hr/>

                                                    <label>Tipo do Papel:</label>
                                                    <span style="font-size: 1.2em">Fotográfico</span>
                                                    <hr/>

                                                    <label>Acabamento:</label>
                                                    <span style="font-size: 1.2em">Brilho</span>
                                                    <hr/>

                                                    <label>Correção:</label>
                                                    <span style="font-size: 1.2em">Com Correção</span>
                                                    <hr/>

                                                    <label>Fotos à Revelar:</label>
                                                    <span style="font-size: 1.2em">6 Fotos</span>
                                                    <hr/>

                                                    <label>Valor Total:</label>
                                                    <span style="font-size: 1.5em">R$ 5,40</span>
                                                    <hr/>

                                                    <a href="#" class="btn btn-outlined btn-primary">Fazer Login</a>
                                                    <a href="#" class="btn btn-outlined btn-primary">Criar Cadastro</a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab7">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <br/>
                                            <div class="alert alert-warning center">
                                                <h4>REALIZAR O PAGAMENTO DA SUA REVELAÇÃO</h4>
                                                Realize de forma segura o pagamento da sua revelação com o PAGSEGURO
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="#" onclick="enviaPagseguro()" class="btn btn-success btn-block btn-lg"><i class="fa fa-money"></i> PAGAR COM PAGSEGURO</a>
                                                    <input type="hidden" name="code" id="code" value="" />

<!--                                                    <script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>-->
                                                    <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>

                                                    <div class="col-md-12">
                                                        <br/>
                                                        <a href="https://pagseguro.uol.com.br/registration/registration.jhtml?tipo=cadastro#!comprador" target="_blank">Clique Aqui</a> para criar uma conta no PAGSEGURO!
                                                        <br/><br/>
                                                    </div>
                                                    <img class="img-responsive" src="https://stc.pagseguro.uol.com.br/public/img/banners/pagamento/todos_estatico_550_100.gif" alt="Logotipos de meios de pagamento do PagSeguro" title="Este site aceita pagamentos com as principais bandeiras e bancos, saldo em conta PagSeguro e boleto."/>                        

                                                    <script type="text/javascript">
                                                        function enviaPagseguro() {
                                                            $.noConflict();
                                                            
                                                            $.post('include/pagseguro.php', '', function (data) {
                                                                $('#code').val(data);
                                                                $('#comprar').submit();
                                                            });
                                                        }
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br/><br/>

                                </div>
                            </div>
                        </div>                                                
                    </div><!--/.row-->                                      
                </div>
            </section><!--/#blog-->
        </div>
        <?php require 'footer.php'; ?>
    </body>
</html>