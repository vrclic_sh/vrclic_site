<!DOCTYPE html>
<html lang="pt">
    <?php require 'header.php'; ?>
    <body>

        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <?php require 'include/menu.php'; ?>
        </header><!--/header-->

        <section id="single-page-slider" class="no-margin">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="center gap fade-down section-heading">
                                        <h2 class="main-title">NOTÍCIAS</h2>
                                        <hr>
                                        <p>NOVIDADES NO MUNDO DA FOTOGRAFIA.</p>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div><!--/.item-->
                </div><!--/.carousel-inner-->
            </div><!--/.carousel-->
        </section><!--/#main-slider-->

        <div id="content-wrapper">


            <section id="blog" class="white">
                <div class="container">                                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="post">
                                <div class="post-img-content">
                                    <img src="https://abrilexame.files.wordpress.com/2018/02/selfie.jpg?quality=70&strip=info&resize=680,453" class="img-responsive" />
                                    <div class="overlay">
                                        <a class="preview btn btn-outlined btn-primary" href="#"><i class="fa fa-link"></i></a>          
                                    </div>
                                </div>
                                <div class="content">
                                    <h2 class="post-title">Cultura da selfie leva museus a autorizar fotografias</h2>
                                    <div class="author">
                                        <i class="fa fa-clock-o"></i> <time datetime="2014-01-20">12/11/2018</time>
                                    </div>
                                    <div>
                                        O comportamento em museus sempre foi sinônimo de restrição, mas a regra de não tirar fotografias foi totalmente eliminada por alguns museus
                                    </div>
                                    <div class="read-more-wrapper">
                                        <a href="noticiaLer.php" class="btn btn-outlined btn-primary">Leia Mais</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="post">
                                <div class="post-img-content">
                                    <img src="https://abrilexame.files.wordpress.com/2018/10/gettyimages-1052757074.jpg?quality=70&strip=info&resize=680,453" class="img-responsive" />
                                    <div class="overlay">
                                        <a class="preview btn btn-outlined btn-primary" href="#"><i class="fa fa-link"></i></a>          
                                    </div>
                                </div>
                                <div class="content">
                                    <h2 class="post-title">Foto de jovem palestino em protesto na Faixa de Gaza viraliza. Entenda</h2>
                                    <div class="author">
                                        <i class="fa fa-clock-o"></i> <time datetime="2014-01-20">10/11/2018</time>
                                    </div>
                                    <div>
                                        Foto de jovem manifestante registrada no início da semana na Faixa de Gaza foi comparada com obra do francês Eugène Delacroix
                                    </div>
                                    <div class="read-more-wrapper">
                                        <a href="noticiaLer.php" class="btn btn-outlined btn-primary">Leia Mais</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>                              
                </div>
            </section>


            <!--                            <div class="row fade-up">
                                            <div class="col-md-12">
                                                <div class="testimonial-list-item">
                                                    <a href="#">
                                                        <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/80x80">
                                                        <blockquote>
                                                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</h4>
                                                            <small>08/11/2018</small>                                               
                                                        </blockquote>
                                                    </a>                                        
                                                </div>
                                                <div class="testimonial-list-item">
                                                    <a href="#">
                                                        <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/80x80">
                                                        <blockquote>
                                                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</h4>
                                                            <small>08/11/2018</small>                                               
                                                        </blockquote>
                                                    </a>                                        
                                                </div>
                                                <div class="testimonial-list-item">
                                                    <a href="#">
                                                        <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/80x80">
                                                        <blockquote>
                                                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</h4>
                                                            <small>08/11/2018</small>                                               
                                                        </blockquote>
                                                    </a>                                        
                                                </div>
                                                <div class="testimonial-list-item">
                                                    <a href="#">
                                                        <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/80x80">
                                                        <blockquote>
                                                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</h4>
                                                            <small>08/11/2018</small>                                               
                                                        </blockquote>
                                                    </a>                                        
                                                </div>
                                                <div class="testimonial-list-item">
                                                    <a href="#">
                                                        <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/80x80">
                                                        <blockquote>
                                                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</h4>
                                                            <small>08/11/2018</small>                                               
                                                        </blockquote>
                                                    </a>                                        
                                                </div>
                                                <div class="testimonial-list-item">
                                                    <a href="#">
                                                        <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/80x80">
                                                        <blockquote>
                                                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante a ante.</h4>
                                                            <small>08/11/2018</small>
            
                                                        </blockquote>
                                                    </a>                                        
                                                </div>
                                                <div class="testimonial-list-item">
                                                    <a href="#">
                                                        <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/80x80">
                                                        <blockquote>
                                                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</h4>
                                                            <small>08/11/2018</small>                                               
                                                        </blockquote>
                                                    </a>                                        
                                                </div>
                                                <div class="testimonial-list-item">
                                                    <a href="#">
                                                        <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/80x80">
                                                        <blockquote>
                                                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</h4>
                                                            <small>08/11/2018</small>                                               
                                                        </blockquote>
                                                    </a>                                        
                                                </div>
            
            
                                            </div>
                                        </div>-->


        </div>

        <?php require 'footer.php'; ?>

    </body>
</html>