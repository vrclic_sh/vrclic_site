<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="index.php"><h1>FOTO <b>IRIS</b></h1></a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li><a href="sobre.php">Sobre</a></li>
            <li><a href="servicos.php">Serviços</a></li>
            <li><a href="eventos.php">Eventos</a></li>
            <li><a href="noticias.php">Notícias</a></li>
            <li><a href="contato.php">Contato</a></li> 
            <!--            <li class="dropdown active">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="icon-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="project-item.html">Project Single</a></li>
                                <li><a href="blog-item.html">Blog Single</a></li>
                                <li class="active"><a href="404.html">404</a></li>
                            </ul>
                        </li>-->
            <li><a href="uploadFotos.php">Revelar Fotos</a></li>
            <li><a href="loginRestrito.php">*Restrito</a></li>             
        </ul>
    </div>
</div>