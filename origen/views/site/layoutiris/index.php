<!DOCTYPE html>
<html lang="pt">
    <?php require 'header.php'; ?>
    <body>

        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <?php require 'include/menu.php'; ?>
        </header><!--/header-->

        <section id="main-slider" class="no-margin">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">                                    
                                    <div class="carousel-content center centered">
                                        <div class="logoIndex">
                                            <img src="../../../res/site/layoutiris/images/NovaPropostaLogoB.png" class="img-responsive"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.item-->
                </div><!--/.carousel-inner-->
            </div><!--/.carousel-->
        </section><!--/#main-slider-->

        <div id="content-wrapper">
            <section id="about-us" class="white">
                <div class="container">                  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="center gap fade-down section-heading">
                                <h2>AGILIDADE COM QUALIDADE!</h2>                                
                                <p>Há 20 anos no ramo da fotografia e de cobertura de eventos, o Foto Iris se destaca pela qualidade e pelo bom atendimento aos clientes, oferecendo um serviço rápido e de boa qualidade, contando com equipamentos modernos e seus funcionários estão constantemente se aprimorando na arte de fotografar.</p>                               
                                <br/>
                                <a class="btn btn-outlined btnf btn-primary" href="sobre.php">SAIBA MAIS</a>
                            </div>                
                        </div>
                    </div>                                       
                </div>      
            </section>


            <section id="single-quote" class="divider-section"> 	            	        
                <div class="container">                   
                    <div class="row">                        
                        <div class='col-md-offset-2 col-md-8 fade-up'>                            
                            <div class="center gap fade-down section-heading">
                                <h2 class="main-title">REVELE SUAS FOTOS COM A GENTE!</h2>
                                <hr>
                                <p>Conheça nosso serviço de revelação de fotos online!</p>

                                <p>Envie suas fotos, escolha os tamanhos, realize o pagamento, depois venha retira-las.</p>

                                <a class="btn btn-outlined btnf btn-primary" href="uploadFotos.php">Enviar Fotos</a>
                            </div>                         
                        </div>
                    </div>                    
                </div>
            </section>

            <section id="services" class="white">
                <div class="container">
                    <div class="gap"></div> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="center gap fade-down section-heading">
                                <h2 class="main-title">Nossos Serviços</h2>
                                <hr>
                                <p>Capturando grandes momentos com qualidade!</p>
                            </div>                
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-camera fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Fotografia</h3>
                                    <p>Nay middleton him admitting consulted and behaviour son household. Recurred advanced he oh together entrance speedily suitable. Ready tried gay state fat could boy its among shall.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-camera fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Fotografia Comercial</h3>
                                    <p>Unfeeling agreeable suffering it on smallness newspaper be. So come must time no as. Do on unpleasing possession as of unreserved.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-ticket fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Cobertura de Eventos</h3>
                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                    </div><!--/.row-->
                    <div class="gap"></div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-camera fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Foto Estúdio</h3>
                                    <p>Yet joy exquisite put sometimes enjoyment perpetual now. Behind lovers eat having length horses vanity say had its</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-cogs fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Restauração de Foto</h3>
                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-camera fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Foto para Documentos</h3>
                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                    </div><!--/.row-->
                </div>
                <div class="gap"></div>               
            </section>


            <section id="testimonial-carousel" class="divider-section">
                <div class="gap"></div>
                <div class="container">
                    <div class="row">
                        <div class="center gap fade-down section-heading">
                            <h2 class="main-title">DEIXE UM DEPOIMENTO!</h2>
                            <hr>
                            <p>CONTE-NOS SOBRE SUA EXPERIÊNCIA COM NOSSO TRABALHO. <br/>A NOSSA BUSCA É PARA QUE VOCÊ SEMPRE TENHA O MELHOR EM QUALIDADE!</p>
                            <div class="gap"></div>
                        </div>                         
                        <div class='col-md-offset-2 col-md-8 fade-up'>
                            <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                                <!-- Bottom Carousel Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#quote-carousel" data-slide-to="1"></li>
                                    <li data-target="#quote-carousel" data-slide-to="2"></li>
                                </ol>                                
                                <!-- Carousel Slides / Quotes -->
                                <div class="carousel-inner">                                
                                    <!-- Quote 1 -->
                                    <div class="item active">
                                        <blockquote>
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
                                                    <small>Someone famous</small>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!-- Quote 2 -->
                                    <div class="item">
                                        <blockquote>
                                            <div class="row">                                                
                                                <div class="col-sm-9">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
                                                    <small>Someone famous</small>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!-- Quote 3 -->
                                    <div class="item">
                                        <blockquote>
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.</p>
                                                    <small>Someone famous</small>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                </div>                                     
                            </div> 
                        </div>
                    </div>
                    <div class="gap"></div>
                </div>
            </section>

            <section id="portfolio" class="white">
                <div class="container">
                    <div class="gap"></div> 
                    <div class="center gap fade-down section-heading">
                        <h2 class="main-title">Nossos Eventos</h2>
                        <hr>
                        <p>Conheça um pouco de nosso trabalho!</p>
                    </div>                   

                    <ul class="portfolio-items col-4 isotope fade-up">
                        <li class="portfolio-item apps isotope-item">
                            <div class="item-inner">
                                <img src="http://placehold.it/800x600" alt="">
                                <h5>Casamento Fulano e Beltrana</h5>                                
                                <div class="overlay">
                                    <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>             
                                </div>           
                            </div>           
                        </li><!--/.portfolio-item-->
                        <li class="portfolio-item joomla bootstrap isotope-item">
                            <div class="item-inner">
                                <img src="http://placehold.it/800x600" alt="">
                                <h5>Casamento Fulano e Beltrana</h5>
                                <div class="overlay">
                                    <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>              
                                </div>           
                            </div>           
                        </li><!--/.portfolio-item-->
                        <li class="portfolio-item bootstrap wordpress isotope-item">
                            <div class="item-inner">
                                <img src="http://placehold.it/800x600" alt="">
                                <h5>Casamento Fulano e Beltrana</h5>
                                <div class="overlay">
                                    <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>        
                                </div>           
                            </div>           
                        </li><!--/.portfolio-item-->
                        <li class="portfolio-item joomla wordpress apps isotope-item">
                            <div class="item-inner">
                                <img src="http://placehold.it/800x600" alt="">
                                <h5>Casamento Fulano e Beltrana</h5>
                                <div class="overlay">
                                    <a class="preview btn btn-outlined btn-primary" href="http://placehold.it/800x600" rel="prettyPhoto"><i class="fa fa-eye"></i></a>          
                                </div>           
                            </div>           
                        </li><!--/.portfolio-item-->
                        <!--/.portfolio-item-->
                    </ul>

                    <div class="col-md-12 center">
                        <a class="btn btn-outlined btnf btn-primary" href="uploadFotos.php">VER TODOS OS EVENTOS</a>
                    </div>
                </div>
            </section>

        </div>


        <?php require 'footer.php'; ?>

    </body>
</html>

