<!DOCTYPE html>
<html lang="pt">
    <?php require 'header.php'; ?>
    <body>

        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <?php require 'include/menu.php'; ?>
        </header><!--/header-->

        <section id="single-page-slider" class="no-margin">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="center gap fade-down section-heading">
                                        <h2 class="main-title">Nossos Serviços</h2>
                                        <hr>
                                        <p>Capturando grandes momentos com qualidade!</p>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div><!--/.item-->
                </div><!--/.carousel-inner-->
            </div><!--/.carousel-->
        </section><!--/#main-slider-->

        <div id="content-wrapper">
            <section id="services" class="white">
                <div class="container">
                    <div class="gap"></div>             
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-camera fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Fotografia</h3>
                                    <p>Nay middleton him admitting consulted and behaviour son household. Recurred advanced he oh together entrance speedily suitable. Ready tried gay state fat could boy its among shall.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-camera fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Fotografia Comercial</h3>
                                    <p>Unfeeling agreeable suffering it on smallness newspaper be. So come must time no as. Do on unpleasing possession as of unreserved.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-ticket fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Cobertura de Eventos</h3>
                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                    </div><!--/.row-->
                    <div class="gap"></div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-camera fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Foto Estúdio</h3>
                                    <p>Yet joy exquisite put sometimes enjoyment perpetual now. Behind lovers eat having length horses vanity say had its</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-cogs fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Restauração de Foto</h3>
                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                        <div class="col-md-4 col-sm-6">
                            <div class="service-block">
                                <div class="pull-left bounce-in">
                                    <i class="fa fa-camera fa fa-md"></i>
                                </div>
                                <div class="media-body fade-up">
                                    <h3 class="media-heading">Foto para Documentos</h3>
                                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                                </div>
                            </div>
                        </div><!--/.col-md-4-->
                    </div><!--/.row-->
                </div>
                <div class="gap"></div>
<!--                <div class="row">
                    <div class="col-md-12">
                        <div class="center gap fade-down section-heading">
                            <h2 class="main-title">Our Skills</h2>
                            <hr>
                            <p>Of an or game gate west face shed. ﻿no great but music too old found arose.</p>
                        </div>               
                    </div>
                </div>
                <div class="container">     
                    <div class="row">            
                        <div class="col-md-3">
                            <div class="tile-progress tile-red bounce-in">
                                <div class="tile-header">
                                    <h3>Video Editing</h3>
                                    <span>Our cutting room floor is messy.</span>
                                </div>
                                <div class="tile-progressbar">
                                    <span data-fill="65.5%" style="width: 65.5%;"></span>
                                </div>
                                <div class="tile-footer">
                                    <h4>
                                        <span class="pct-counter counter">65.5</span>%
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="tile-progress tile-cyan bounce-in">
                                <div class="tile-header">
                                    <h3>Marketing</h3>
                                    <span>How well we can sell you and your brand.</span>
                                </div>
                                <div class="tile-progressbar">
                                    <span data-fill="98.5%" style="width: 98.5%;"></span>
                                </div>
                                <div class="tile-footer">
                                    <h4>
                                        <span class="pct-counter counter">98.5</span>%
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="tile-progress tile-primary bounce-in">
                                <div class="tile-header">
                                    <h3>Web Development</h3>
                                    <span>We love servers and stuff.</span>
                                </div>
                                <div class="tile-progressbar">
                                    <span data-fill="90%" style="width: 90%;"></span>
                                </div>
                                <div class="tile-footer">
                                    <h4>
                                        <span class="pct-counter counter">90</span>%
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="tile-progress tile-pink bounce-in">
                                <div class="tile-header">
                                    <h3>Coffee</h3>
                                    <span>We done make good joe, though.</span>
                                </div>
                                <div class="tile-progressbar">
                                    <span data-fill="10%" style="width: 10%;"></span>
                                </div>
                                <div class="tile-footer">
                                    <h4>
                                        <span class="pct-counter counter">10</span>%
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>/.row-->
                    <div class="gap"></div>
                </div>
            </section>
        </div>
        <?php require 'footer.php'; ?>

    </body>
</html>