<!DOCTYPE html>
<html lang="pt">
    <?php require 'header.php'; ?>
    <body>

        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <?php require 'include/menu.php'; ?>
        </header><!--/header-->

        <section id="single-page-slider" class="no-margin">
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="center gap fade-down section-heading">
                                        <h2 class="main-title">FOTO IRIS</h2>
                                        <hr>
                                        <p>LABORATÓRIO DIGITAL.</p>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div><!--/.item-->
                </div><!--/.carousel-inner-->
            </div><!--/.carousel-->
        </section><!--/#main-slider-->

        <div id="content-wrapper">
            <section id="about-us" class="white">
                <div class="container">
                    <div class="gap"></div>
                    <div class="row">
                        <div class="col-md-12 fade-up">
                            <div class="center centered">
                                <img src="../../../res/site/layoutiris/images/NovaPropostaLogo.png" style="max-width: 350px"/>    
                            </div>
                            
                            <h3>Agilidade com Qualidade!</h3>
                            <p>Há 20 anos no ramo da fotografia e de cobertura de eventos, o Foto Iris se destaca pela qualidade e pelo bom atendimento aos clientes, oferecendo um serviço rápido e de boa qualidade, contando com equipamentos modernos e seus funcionários estão constantemente se aprimorando na arte de fotografar.</p>

                            <p>Toda dedicação e esforço estão voltados para oferecer o melhor serviço com a qualidade que você, amigo cliente merece.</p>

                            <p>Faça parte desta família, venha registrar os seus melhores momentos conosco.</p>

                        </div>
                        <div class="col-md-4 fade-up">

                        </div>
                    </div>
<!--                    <div class="gap"></div>
                    <div class="row fade-up">
                        <div class="col-md-6">
                            <div class="testimonial-list-item">
                                <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/400x400">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                    <small>Manager at <cite title="Source Title">Company</cite></small>
                                </blockquote>
                            </div>
                            <div class="testimonial-list-item">
                                <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/400x400">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                    <small>Manager at <cite title="Source Title">Company</cite></small>
                                </blockquote>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="testimonial-list-item">
                                <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/400x400">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                    <small>Manager at <cite title="Source Title">Company</cite></small>
                                </blockquote>
                            </div>
                            <div class="testimonial-list-item">
                                <img class="pull-left img-responsive quote-author-list" src="http://placehold.it/400x400">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                    <small>Manager at <cite title="Source Title">Company</cite></small>
                                </blockquote>
                            </div>
                        </div>
                    </div>-->

                    <div class="gap"></div>
                    <div class="center gap fade-down section-heading">
                        <h2 class="main-title">Nossa Equipe</h2>
                        <hr>
                        <p>Profissionais capacitados e Qualificados</p>
                    </div> 
                    <div class="gap"></div>

                    <div id="meet-the-team" class="row">
                        <div class="col-md-3 col-xs-6">
                            <div class="center team-member">
                                <img class="img-responsive img-thumbnail bounce-in" src="http://placehold.it/400x400" alt="">
                                <div class="team-content fade-up">
                                    <h5>Elvis Perreira<small class="role muted">Editor / Fotógrafo</small></h5>
                                    <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                    <a class="btn btn-social btn-facebook" href="#"><i class="fa fa-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="fa fa-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="fa fa-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-xs-6">
                            <div class="center team-member">
                                <img class="img-responsive img-thumbnail bounce-in" src="http://placehold.it/400x400" alt="">
                                <div class="team-content fade-up">
                                    <h5>Jadir<small class="role muted">Diretor / Fotógrafo</small></h5>
                                    <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                    <a class="btn btn-social btn-facebook" href="#"><i class="fa fa-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="fa fa-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="fa fa-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>        
                        <div class="col-md-3 col-xs-6">
                            <div class="center team-member">
                                <img class="img-responsive img-thumbnail bounce-in" src="http://placehold.it/400x400" alt="">
                                <div class="team-content fade-up">
                                    <h5>Jéssica<small class="role muted">Fotógrafa</small></h5>
                                    <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                    <a class="btn btn-social btn-facebook" href="#"><i class="fa fa-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="fa fa-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="fa fa-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>        
                        <div class="col-md-3 col-xs-6">
                            <div class="center team-member">
                                <img class="img-responsive img-thumbnail bounce-in" src="http://placehold.it/400x400" alt="">
                                <div class="team-content fade-up">
                                    <h5>Fulano<small class="role muted">Fotógrafo</small></h5>
                                    <p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>
                                    <a class="btn btn-social btn-facebook" href="#"><i class="fa fa-facebook"></i></a> <a class="btn btn-social btn-google-plus" href="#"><i class="fa fa-google-plus"></i></a> <a class="btn btn-social btn-twitter" href="#"><i class="fa fa-twitter"></i></a> <a class="btn btn-social btn-linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/#meet-the-team-->                  
                    <div class="gap"></div>   
                </div>      
            </section>
        </div>
        <?php require 'footer.php'; ?>

    </body>
</html>